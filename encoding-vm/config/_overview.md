**aws-credentials**

This file stores the AWS user credentials for 'olv-qa' user, under the profile
name 'rogercpa-usb'. It's used by Node.js Gulp scripts within the encoder
systems to download / upload video assets.

These can be managed in the AWS > IAM Security pages, for the 'olv-qa' user
within the AWS Console.

**olv-encoder.pem**

This is a private key used to access AWS servers created by the
bin/ec2-create-instance.sh script. You can use the key manually to access the
server via SSH.

    $ ssh -i config/olv-encoder.pem ubuntu@SERVER

It can be managed under the AWS > EC2 Keys section of the AWS Console.
