Overview
========

We create servers from the CLI on our workstations. We login and then run gulp to execute the batch.

Required AWS Credentials
========================

The following two credentials need to be obtained before starting.

1. API access (AWS IAM user olv-vagrant): Used to create a VM.

Vagrant will use the profile 'olv-vagrant'

2. SSH access (AWS EC2 Keypair olv-encoder) (olv-encoder.pem): Used to login to the VM.

We can leave this file in the config directory, use it via the -i flag for SSH commands.

    $ ssh -i config/olv-encoder.pem ubuntu@dest

AWS CLI
=======

### Install

We will require awscli command line tools, there are a variety of ways to install these tools.

[Official install instructions](http://docs.aws.amazon.com/cli/latest/userguide/installing.html)

- **Install on Mac using Homebrew:** ```brew install awscli```
- **Install using pip:** ```pip install awscli [--upgrade]```

Result: Ensure you have ```aws``` availabe by checking it's version

    $ aws --version
    > aws-cli/1.11.28 Python/2.7.10 Darwin/16.1.0 botocore/1.4.85

**Setup authorization to AWS EC2**

Ensure you obtain the 'olv-vagrant' EC2 IAM user credentials (key/secret) from an admin, then run the following. That
user needs to have permissions to create servers in EC2, as well as remove servers created by them.

    $ aws configure --profile olv-vagrant
    > AWS Access Key ID [None]: OLV_KEY_HERE
    > AWS Secret Access Key [None]: OLV_SECRET_HERE
    > Default region name [None]: us-west-1
    > Default output format [None]: ENTER

Result: Ensure that the two files on your system have been created, with content matching your auth info.

    $ cat ~/.aws/config
    $ cat ~/.aws/credentials

_Note that the scripts below reference the profile 'olv-vagrant'_

Creating EC2 Instance Servers
=============================

@NOTE: IMPORTANT - Currently: we need to delete servers manually from within the AWS Console when
we're done with them. Any instance created with these scripts will have the Name **olv-encoder**.

Create an instance

    $ bin/ec2-create-instance.sh
    > output will == an INSTANCE_ID and PUBLIC_ADDRESS, save these

Upload files (and/or sync local file changes to the server)

    $ bin/ec2-upload-files.sh $PUBLIC_ADDRESS
    > ... output from rsync ...

Provision the instance

    $ ssh -i config/olv-encoder.pem ubuntu@PUBLIC_URL
    > ... logged into server ...
    $ cd encoder
    $ ./bin/provision-ubuntu-14.04.sh

Start batch processing

    $ ssh -i config/olv-encoder.pem ubuntu@PUBLIC_URL
    > ... logged into server ...
    $ cd encoder
    $ AWS_PROFILE='rogercpa-usb' gulp --config=fixtures/<path to fixtures file for processing>

If there are no errors, you'll be able to see the logs indicating progress of the video processing. The
encoding step will take about 20-40 mins depending on the video, and you'll be able to see the time logs
to the left of the terminal output.

It may be helpful to use tmux to connect to the server when starting a batch, so that your internet
connection does not hinder the batch.

Start a new session

    $ ssh -i config/olv-encoder.pem ubuntu@DEST -t tmux

Attach to an existing session

    $ ssh -i config/olv-encoder.pem ubuntu@DEST -t tmux a
