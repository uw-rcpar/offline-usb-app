// Usage:
//
// AWS_PROFILE='rogercpa-usb' gulp --config=fixtures/settings-rcpar.courseware-v2.1.2016-regular.json
// AWS_PROFILE='rogercpa-usb' gulp --config=fixtures/settings-rcpar.courseware-v2.1.2016-cram.json
// AWS_PROFILE='rogercpa-usb' gulp --config=fixtures/settings-rcpar.courseware-v2.1.2016-test.json
//
// AWS_PROFILE='rogercpa-usb' gulp test --config=fixtures/settings-rcpar.courseware-v2.1.2016-test.json
//
//
// @todo:
// - cleanup all files should be on (test this in large batch)
// - reset the 'dry_run' check in the upload function
//

var gulp = require('gulp'),
    gutil = require('gulp-util'),
    // plumber = require('gulp-plumber'),
    argv = require('yargs').argv,
    fs = require('fs'),
    path = require('path'),
    mkdirp = require('mkdirp'),
    remoteSrc = require("gulp-remote-src"),
    del = require('del'),
    async = require('async'),
    aws = require('aws-sdk'),
    // ffmpeg = require('gulp-fluent-ffmpeg'),
    ffmpeg = require('fluent-ffmpeg'),
    shell = require('gulp-shell')
    // exec = require('gulp-exec')
    ;

var aws_config = new aws.Config({
    httpOptions: {
        timeout:1200000
    }
});

var s3up = require('gulp-s3-upload')(aws_config);

// this file should come from http://rogercapreview.com/admin/settings/rcpar/rebuild-course-stats/json,
// then the file should be split into 2 files - one for regular and one for cram courses.
// this is simply so we are able to batch these in separate processes, and upload to separate buckets
var fixtures_file = getSettingsFilePath(),
    config = JSON.parse(fs.readFileSync(fixtures_file)).config;

// var source_base = 'http://www.sample-videos.com/video/mp4/720/';
// var source_base = 'https://s3.amazonaws.com/rcpar.courseware-v2.1.2016/regular_course/@section@/web/'
// var source_base = 'AUD-00.01.mp4'
var source_base = config.download_url_template;
// var upload_base = path.join(config.upload_;

var dirs = {
    // keydir is just named in a way that we can see when the key was created. we'll commit these to the repo
    // so that we never loose the keys easily, since they will be required to use data on the USB drives
    keys: "keys/20160524-v1",
    mp4: "build/src/mp4/",
    webm: "build/src/webm/" + config.upload_prefix + "/",
    webm_crypt: "build/output/webm_crypt/" + config.upload_prefix + "/"
}

// set to true will skip the download/transcode/encrypt steps, the result
// will be output to the terminal of the files that would be read / created
// good way to test closures / scope is proper in the flow
var dry_run = false;
// dry_run = true;

// delete source files, this helps prevent disk usage growing past a couple vidoes
// when we run with lots of files. set this to false to preserve artifacts when debugging
var cleanup_src = false;
cleanup_src = true;

gulp.task('test', function() {

    var sections = JSON.parse(fs.readFileSync(fixtures_file)).videos,
        config = JSON.parse(fs.readFileSync(fixtures_file)).config,
        log = gutil.log,
        s3 = new aws.S3(),
        params;

    log("config.download_url_template: " + config.download_url_template);
    log("");
    for (var section_key in sections) {
        var section = sections[section_key];
        // log('section: ' + section);
        log('section_key: ' + section_key);
    }
    log("");
    log("Checking credentials to AWS");


    // var params = {
    //   Bucket: 'STRING_VALUE', /* required */
    //   Delimiter: 'STRING_VALUE',
    //   EncodingType: 'url',
    //   Marker: 'STRING_VALUE',
    //   MaxKeys: 0,
    //   Prefix: 'STRING_VALUE',
    //   RequestPayer: 'requester'
    // };

    params = {
        Bucket: 'rcpar.courseware-v2.2.2016',
        Prefix: 'regular_course'
    };

    s3.listObjects(params, function(err, data) {
        if (err) log('error in listObjects', err.code); // an error occurred
        else     log('Found ' + data.length + ' item(s) in the bucket');
    });

    params = {
        Bucket: 'rcpar.courseware-v2.2.2016',
        // Prefix: 'cram_course',
        Key: 'regular_course/aud/web/AUD-0.01_test.mp4'
    };

    log('Checking for file in AWS');
    log('Bucket: ' + params.Bucket);
    log('Key: ' + params.Key);
    s3.headObject(params, function (err, metadata) {
        if (err && err.code === 'NotFound') {
            // Handle no object on cloud here
            log('File not found.');
        } else {
            // s3.getSignedUrl('getObject', params, function();
            log('Meta data on the file');
            log(metadata);
        }
    });

});

// default task is to batch process (download, encode, upload) the videos
gulp.task('default', function() {
    var sections = JSON.parse(fs.readFileSync(fixtures_file)).videos,
        config = JSON.parse(fs.readFileSync(fixtures_file)).config,
        log = gutil.log,
        s3 = new aws.S3(),
        aws_pre_exist;

    // create dirs we'll use
    for (var dir in dirs) {
        mkdirp.sync(path.join(dirs[dir]));
        log("Created directory", dirs[dir]);
    }

    // we'll need to wrap this in an event callback, we need to first get a list of
    // all files in the upload target bucket, so we can determine syncronously if
    // we need to process a video. this is because buildAsyncSeries needs to run syncrounously
    // so it can build the async series which then runs in an async fashion
    params = {
        Bucket: config.upload_bucket,
        Prefix: config.upload_prefix
    };

    s3.listObjects(params, function(err, data) {
        log('List of existing files from server...');
        log(data);
        log('err:', err);

        if (err) {
            log('Error in AWS.S3.listObjects() call. (When checking for pre existing webm files.');
            log(err);
        } else {
            // generate a flat array of the keys, easier to search later with Array.include(val)
            var files = [],
                contents = data.Contents,
                data_count = contents.length,
                i;
            for (i = 0; i < data_count; i++) {
                log('Adding ' + contents[i].Key + ' to list of existing files');
                files.push(contents[i].Key);
            }

            log('Obtained list of pre existing files in AWS, starting queue generation.');
            log(files);
            log('\tList generated from ' + params.Bucket + '/' + params.Prefix);
            // setup the batch to run, using an async series to drive the batch
            var series = buildAsyncSeries(sections, config, files);

            // series should be loaded, let's start processing
            async.series(series, function(err, results){
                log('Series completed', results);
            });
        }
    });
});

gulp.task('generate-keys', function() {

    var sections = JSON.parse(fs.readFileSync(fixtures_file)).videos,
        log = gutil.log;

    // just a small video we can use to create our keys. this could be a video of our own also
    var dummy_video = path.join(dirs.keys, '..', 'sample-video.webm');

    // track the key generate process, once runs at a time
    var series = [];

    // used to backup the key files first if they exist, (dir.backup.timestamp-seconds)
    var backup_keys_dir = path.join(dirs.keys + '.backup.' + Math.floor(Date.now() / 1000)),
        current_backup_dir = fs.statSync(path.join(dirs.keys));
    if (current_backup_dir.isDirectory()) {
        // move backup dir
        fs.renameSync(dirs.keys, backup_keys_dir);
    }

    mkdirp(path.join(dirs.keys));

    // there are a few ways we can generate keys...
    //
    // run webm to just create a key, once for each section then
    // delete the sample encrypted video after
    //
    // use hexdump from generated file
    // hexdump -e '16/1 "%02x " "\n"'   AUD.key
    //     result: 3a 5a 4d 4d 78 93 72 e5 9b 11 25 75 e8 e7 ee 9b
    //
    // or from scratch we can create keys from 22 random strings
    for (var section_key in sections) {
        var callbackBuilder_GenerateKey = function() {

            // important to define the closure vars here, or they will all equal
            // whatever they were in the last loop iteration when the callback
            // function actually runs
            var key_file = path.join(dirs.keys, section_key + '.key');
            var video_in = dummy_video;
            var video_out = path.join(dummy_video + '.crypt');
            var base64_raw_string = generateBase64();

            return function (callback) {

                log('Encrypting video to generate key');
                log('\t', section_key);
                log('\t', key_file);

                // run webm crypt here to create key
                log('\t', 'executing shell on', video_in);


                return gulp.src(video_in, {read: false})
                    .pipe(shell([
                        'echo <%= base64_string %>==',
                        'echo <%= base64_string %>== | base64 --decode > <%= key_file %>',
                        'echo <%= base64_string %>== > <%= key_file %>.txt'
                    ], {
                        templateData: {
                            // video_in: video_in,
                            // video_out: video_out,
                            key_file: key_file,
                            base64_string: base64_raw_string
                        },
                        verbose: true
                    }))
                    .on('finish', function(){
                        log('End generate key for ' + section_key);
                        log('\t', 'section key:', section_key);

                        // delete the sample encrypted file
                        del(path.join(video_out));

                        callback(null, key_file);
                    });
            }
        }

        var cb_generate_key = callbackBuilder_GenerateKey();
        series.push(cb_generate_key);
    }

    async.series(series, function(err, results){
        log('Key generation completed', results);
    });
});

// utility to extract a video list from the json data
function buildAsyncSeries(sections, config, existing) {
    var series = [],
        log = gutil.log;

    // process sections
    for (var section_key in sections) {
        var section = sections[section_key],
            chapters = section.chapters;

        log('Loading section', section_key);

        for (var chapter_key in chapters) {
            var chapter = chapters[chapter_key],
                topics = chapter.topics;

            log('Loading chapter', chapter_key);

            for (var topic_key in topics) {
                var video_file = topics[topic_key].video_file,
                    webm_file = video_file + '.crypt.webm';
                    topic = topics[topic_key],
                    section_lower = getAwsBuckSubFolderName(section_key),
                    // aws_key_base = upload_base.replace('@section@', section_lower),
                    aws_key_base = path.join(config.upload_prefix),
                    aws_key = path.join(aws_key_base, webm_file);

                log('Checking for encrypted file: ' + aws_key);
                log('\taws_key_base: ' + aws_key_base);
                log('\taws_key: ' + aws_key);
                if (existing.indexOf(aws_key) === -1) {
                    log('\tencrypted file does not exist yet, continue.');
                } else {
                    log('\tencrypted file exists, skip processing.');
                    continue;
                }

                // // skip this topic if it's webm_crypt exists already (local file)
                // try {
                //     var final_file_artifact = fs.statSync(path.join(dirs.webm_crypt, webm_file));
                //     if (final_file_artifact.isFile()) {
                //         log("File exists already, skip.", path.join(dirs.webm_crypt, webm_file));
                //         continue;
                //     }
                // }
                // catch(err) {
                //     log("File does not exist.", path.join(dirs.webm_crypt, video_file));
                // }

                // create an enclouser for our callback function
                var callbackBuilder_DownloadFile = function() {
                    var file = video_file,
                        topic = topics[topic_key],
                        section_lower = getAwsBuckSubFolderName(section_key);

                    // return this function, which includes the snapshot of vars
                    // from above in the callback builder scope
                    return function (callback) {

                        log('Downloading video');
                        log('\t', topic.title);
                        log('\t', topic.video_file);

                        var base = source_base.replace('@section@', section_lower);

                        if (dry_run) {
                            log('\t', 'DRY RUN:', 'downloaded', base + topic.video_file);
                            callback(null, topic.video_file);
                            return;
                        }

                        // delete the mp4 file first
                        del(path.join(dirs.mp4, topic.video_file));

                        var stream = remoteSrc(topic.video_file, {base: base})
                            .on('error', function(err) {
                                log('========= Logging Error =========');
                                log(err);
                                callback();
                            })
                            // .pipe(plumber({
                            //     // log error to the screen
                            //     errorHandler: function(err) {
                            //         log("");
                            //         log(err);
                            //         log("");
                            //     }
                            // }))
                            .pipe(gulp.dest(dirs.mp4))
                            .on('end', function(){
                                callback(null, topic.video_file);
                            });
                    }
                }

                // create an enclouser for our callback function
                var callbackBuilder_ConvertToWebm = function() {
                    var file = video_file,
                        topic = topics[topic_key];

                    // return this function, which includes the snapshot of vars
                    // from above in the callback builder scope
                    return function (callback) {

                        var mp4_filepath = path.join(dirs.mp4, topic.video_file)
                            webm_filepath = path.join(dirs.webm, topic.video_file + '.webm');

                        log('Transcoding video');
                        log('\t', 'src', mp4_filepath);
                        log('\t', 'dest', webm_filepath);

                        if (dry_run) {
                            log('\t', 'DRY RUN:', 'transcoded', webm_filepath);
                            callback(null, webm_filepath);
                            return;
                        }

                        // delete the webm file first
                        del(webm_filepath);

                        // show source info
                        // ffmpeg.ffprobe(mp4_filepath, function(err, metadata) {
                        //     log(metadata);
                        // });

                        // cli test about 140% of the original mp4 size
                        // ffmpeg -i mp4/AUD-0.01.mp4 -codec:v libvpx -quality good -cpu-used 0 -b:v 500K -maxrate 500k -bufsize 1000k -qmin 10 -qmax 42 -vf scale=-1:720 -threads 4 -c:a libvorbis AUD-0.01.webm
                        // close to 105% of the original mp4 size, and using the same bitrate settings / size seen of the original
                        // ffmpeg -i mp4/AUD-0.01.mp4 -codec:v libvpx -quality good -cpu-used 0 -b:v 450K -maxrate 856k -bufsize 900k -qmin 10 -qmax 42 -vf scale=-1:480 -threads 4 -c:a libvorbis AUD-0.01.webm

                        // important notes: the 'threads' should match the max available threads on the computer
                        ffmpeg(mp4_filepath)
                            .videoCodec('libvpx')
                            .videoBitrate('450k')
                            .size('?x480')
                            // setting size() to '?x720' equals
                            // ... 'maintain aspect ration and set height to 720'
                            // ... which is the same as setting cli scale option to 1:720
                            // .aspect('16:9')
                            .outputOptions(['-quality good', '-cpu-used 0', '-qmin 10', '-qmax 42', '-maxrate 856k', '-bufsize 900k', '-threads 4'])
                            .audioCodec('libvorbis')
                            .audioBitrate('128k')
                            .on('error', function(err, stdout, stderr) {
                                log('Cannot process video: ' + err.message);
                                callback();
                            })
                            .on('stderr', function(stderrLine) {
                                // this isn't error always, it's generally status info
                                // log('ffmpg: ' + stderrLine);
                            })
                            .on('end', function(stdout, stderr) {
                                log('\t', 'transcoding mp4 to webm succeeded');
                                // log('\t', '@TODO delete src/mp4');

                                if (cleanup_src) {
                                    del(mp4_filepath);
                                }

                                callback(null, webm_filepath);
                            })
                            .save(webm_filepath)
                            ;
                    }
                }

                // create an enclouser for our callback function
                var callbackBuilder_EncryptWebm = function() {
                    var file = video_file,
                        topic = topics[topic_key],
                        section_id = section_key;

                    // return this function, which includes the snapshot of vars
                    // from above in the callback builder scope
                    return function (callback) {

                        // important to define the closure vars here, or they will all equal
                        // whatever they were in the last loop iteration when the callback
                        // function actually runs
                        var webm_key_id = section_id,
                            webm_key_filename = section_id + '.key',
                            key_file = path.join(dirs.keys, webm_key_filename),
                            video_in = path.join(dirs.webm, topic.video_file + '.webm')
                            video_out = path.join(dirs.webm_crypt, topic.video_file + '.crypt.webm');

                        log('Encrypting video');
                        log('\t', 'key_file', key_file);
                        log('\t', 'video_in', video_in);
                        log('\t', 'video_out', video_out);


                        if (dry_run) {
                            log('\t', 'DRY RUN:', 'encoded', video_out);
                            callback(null, video_out);
                            return;
                        }

                        // delete existing encrypted webm file first if it exists
                        del(video_out);

                        // run webm crypt here to create key
                        return gulp.src(video_in, {read: false})
                            .on('error', function(err) {
                                log('========= Logging Error =========');
                                log(err);
                                callback();
                            })
                            .pipe(shell([
                                // 'echo "webm_crypt -i <%= video_in %> -o <%= video_out %> -video_options base_file=<%= key_file %>,content_id=<%= key_file %>"',
                                'webm_crypt -i <%= video_in %> -o <%= video_out %> -video_options base_file=<%= key_file %>,content_id=<%= key_file %>'
                            ], {
                                templateData: {
                                    video_in: video_in,
                                    video_out: video_out,
                                    key_file: key_file
                                },
                                verbose: true
                            }))
                            .on('finish', function(){
                                log('\t', 'encrypting finished');
                                // log('\t', video_out);

                                // delete the src video
                                if (cleanup_src) {
                                    del(video_in);
                                }

                                callback(null, video_out);
                            });
                    }
                }

                // create an enclouser for our callback function
                var callbackBuilder_UploadWebm = function() {
                    var file = video_file,
                        topic = topics[topic_key],
                        section_id = section_key,
                        key_base = config.upload_prefix,
                        upload_bucket = config.upload_bucket;

                    // return this function, which includes the snapshot of vars
                    // from above in the callback builder scope
                    return function (callback) {

                        // important to define the closure vars here, or they will all equal
                        // whatever they were in the last loop iteration when the callback
                        // function actually runs
                        var webm_key_id = section_id,
                            prefix = key_base,
                            video_upload = path.join(dirs.webm_crypt, topic.video_file + '.crypt.webm'),
                            bucket = upload_bucket;

                        log('Uploading video');
                        log('\t', 'bucket', config.upload_bucket);
                        log('\t', 'video_out', video_upload);

                        if (dry_run) {
                            log('\t', 'DRY RUN:', 'uploaded', video_upload);
                            callback(null, video_upload);
                            return;
                        }

                        // run webm crypt here to create key
                        return gulp.src(video_upload)
                            .pipe(
                                s3up({
                                    Bucket: upload_bucket,
                                    // ACL: 'public-read',
                                    keyTransform: function(relative_filename) {
                                        // log('keyTransform: ' + relative_filename);
                                        return path.join(prefix, relative_filename);
                                    }
                                },{
                                    maxRetrieis: 5
                                })
                            )
                            .on('end', function(){
                                log('\t', 'uploading finished');
                                // log('\t', video_upload);

                                // delete the src video
                                if (cleanup_src) {
                                    del(video_upload);
                                }

                                callback(null, video_upload);
                            });
                    }
                }

                // add our functions to the callback
                var cb_download = callbackBuilder_DownloadFile();
                series.push(cb_download);

                var cb_convert_to_webm = callbackBuilder_ConvertToWebm();
                series.push(cb_convert_to_webm);

                var cb_encrypt_webm = callbackBuilder_EncryptWebm();
                series.push(cb_encrypt_webm);

                var cb_upload_webm = callbackBuilder_UploadWebm();
                series.push(cb_upload_webm);
            }
        }
    }

    return series;
}

// =====================
// = utility functions =
// =====================

/**
 * Create a base64 string minus the otherwise valid + and / chars
 */
function generateBase64() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var len = 22;
    for (var i = 0; i < len; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

/**
 * ensure our arg is used to determine the course type. using a switch to
 * make it easy for us to use a few valid vars in the supplied argument
 * set this when calling gulp:
 *
 *     gulp --app_mode=cram
 *
 * @return String regular_course || cram_course
 */
function getAppMode() {
    var app_mode = argv.mode;

    switch (app_mode) {

        case undefined: // default value
        case 'full':
        case 'regular_course':
            app_mode = 'regular_course';
            break;

        case 'cram':
        case 'cram_course':
            app_mode = 'cram_course';
            break;

        default:
            throw new gutil.PluginError({
                plugin: 'encoding-vm/gulpfile.js',
                message: 'App mode should be either cram_course or regular_course, supplied value: ' + app_mode
            });
    }

    return app_mode;
}

/**
 * Pass the settings file in at run time
 *
 *
 */
function getSettingsFilePath() {
    var config = argv.config,
        config_file;

    config_file = fs.statSync(config);

    if (!config_file.isFile()) {
        log("Settings file not found");
    }

    return config;
}

/**
 * Using the app_mode arg, get the path the fixtures file we'll use
 */
// function getJsonFilenameForCurrentAppMode() {
//     var app_mode = getAppMode(),
//         fixtures_file;
//
//     switch (app_mode) {
//         case 'regular_course':
//             fixtures_file = path.join('fixtures', 'settings-full.json');
//             break;
//         case 'cram_course':
//             fixtures_file = path.join('fixtures', 'settings-cram.json');
//             break;
//     }
//
//     return fixtures_file;
// }

// function getDownloadUrlTemplateForCurrentAppMode() {
//     var app_mode = getAppMode(),
//         source_base;
//
//     switch (app_mode) {
//         case 'regular_course':
//             source_base = 'https://s3.amazonaws.com/rcpar.courseware-v2.1.2016/regular_course/@section@/web/'
//             break;
//         case 'cram_course':
//             source_base = 'https://s3.amazonaws.com/rcpar.courseware-v2.1.2016/cram_course/@section@/web/'
//             break;
//     }
//
//     return source_base;
// }

/**
 * A very specific function, given the section name, we need to clean up
 * that string and get the proper folder name as it's stored in AWS buckets
 * e.g. AUD becomes aud, and AUD-CRAM becomes aud also.
 *
 * This assumes that the folders cram_courses and regular_courses are named
 * like that.
 */
function getAwsBuckSubFolderName(section_key) {
    // ensure the section name is converted to lowercase first
    section_lower = section_key.toLowerCase();

    // sanitize the cram courses
    // @note from (chris buryta): we can probably just always remove any '-cram' string, and make this
    // run without knowledge of the getAppMode(). removing the if logic here to try that out, since
    // the app mode is depricated anyway
    // if (getAppMode() == 'cram_course') {
        // then remove '-cram' from the string
        section_lower = section_lower.replace(/\-cram/g, '');
    // }

    return section_lower;
}

