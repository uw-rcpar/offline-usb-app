Working with Vagrant VM
=======================

Requires: [vagrant](http://vagrantup.com)

- Build the vm with ```vagrant up```,
- From the Host machine, place videos in the same directory or a sub-directory as the Vagrantifle
- SSH into the VM with ```vagrant ssh```
- Access files from in the VM at the top-level direcotry ```/vagrant```

Once inside the VM with access to videos, you can use ```webm_crypt``` and ```ffmpeg``` to complete tasks.
