#!/bin/bash

#
# usage: ./rsync-up.sh ip-10-252-17-25.us-west-1.compute.internal
#

if [ "$#" -ne 1 ]
then
  echo "Usage: ..."
  echo ""
  echo "    rsync-up.sh IP_ADDRESS"
  echo ""
  exit 1
fi

SYNC='rsync -avz -e "ssh -i config/olv-encoder.pem"'
DEST=$1
KEY_PATH=config/olv-encoder.pem

echo "Uploading provisioning scripts to $DEST"

# ssh -i config/olv-encoder.pem ubuntu@$DEST 'mkdir -p encode/'
rsync -avz -e "ssh -i $KEY_PATH" gulpfile.js   ubuntu@$DEST:~/encode/
rsync -avz -e "ssh -i $KEY_PATH" package.json  ubuntu@$DEST:~/encode/
# rsync -avz -e "ssh -i $KEY_PATH" provision.sh  ubuntu@$DEST:~/encode/
rsync -avz -e "ssh -i $KEY_PATH" bin/*         ubuntu@$DEST:~/encode/bin/
rsync -avz -e "ssh -i $KEY_PATH" config/*      ubuntu@$DEST:~/encode/config/
rsync -avz -e "ssh -i $KEY_PATH" fixtures/*    ubuntu@$DEST:~/encode/fixtures/
rsync -avz -e "ssh -i $KEY_PATH" keys/*        ubuntu@$DEST:~/encode/keys/

echo ""
echo "Login to the instance with the following:"
echo ""
echo "    ssh -i config/olv-encoder.pem ubuntu@$DEST"
echo ""
echo "Provision the server with:"
echo ""
echo "    cd ~/encode; bin/provision-ubuntu-14.04.sh"
echo ""
echo "Or combine the two commands, and provision in one fell swoop!"
echo ""
echo "    ssh -i config/olv-encoder.pem ubuntu@$DEST \"cd ~/encode && bin/provision-ubuntu-14.04.sh\""
echo ""
