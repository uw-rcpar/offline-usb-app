#!/bin/bash

 # add access for this server to connect to the limited S3 account olv-qa
mkdir -p ~/.aws
cp -f config/aws-credentials ~/.aws/credentials
# echo "[rogercpa-usb]" > ~/.aws/credentials
# echo "aws_access_key_id = AKIAIYTEGRQQ5RBCRFXQ" >> ~/.aws/credentials
# echo "aws_secret_access_key = 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" >> ~/.aws/credentials

# update cmake to 3.x, required for our builds
sudo add-apt-repository -y ppa:george-edison55/cmake-3.x
sudo apt-get -y update
sudo apt-get -y upgrade

# some handy tools we'll want available
sudo apt-get install -y awscli htop

# install required packages for compiling webm_crypt
sudo apt-get install -y git build-essential libssl-dev software-properties-common

# install required packages so we have ffmpeg
# @see http://www.faqforge.com/linux/how-to-install-ffmpeg-on-ubuntu-14-04/
sudo add-apt-repository -y ppa:mc3man/trusty-media
sudo apt-get update -y
sudo apt-get dist-upgrade -y
sudo apt-get install -y ffmpeg

# ensure our PATH is updated for required libs and bins
echo '' > ~/.bash_profile
echo 'export LD_LIBRARY_PATH="$HOME/src/libwebm"' >> ~/.bash_profile
echo 'export PATH=$PATH:"$HOME/src/webm-tools/webm_crypt"' >> ~/.bash_profile
source ~/.bash_profile

# these are the 2 projects we'll be using
cd ~/
mkdir src
cd src
git clone https://chromium.googlesource.com/webm/libwebm
git clone https://chromium.googlesource.com/webm/webm-tools

# build libwebm
pushd libwebm
make -f Makefile.unix shared
popd

# build webm_crypt
pushd webm-tools/webm_crypt
make
popd

# run a webm_test
webm_crypt -test

# install pip for nodeenv
sudo apt-get install -y python-pip
sudo pip install nodeenv
nodeenv --node=5.10.1 --prebuilt ~/env

# activate nodeenv
source ~/env/bin/activate

# initi nodeenv on load
echo '' >> ~/.bash_profile
echo "source ~/env/bin/activate" >> ~/.bash_profile
source ~/.bash_profile

# ensure we have gulp-cli available
npm install --global gulp-cli

cd ~/encode
npm install
