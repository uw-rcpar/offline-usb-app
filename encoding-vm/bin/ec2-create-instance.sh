#!/bin/bash

echo ""
echo "Using awscli to create server"

# auth profile to use for AWS API access
AWS_PROFILE=${AWS_PROFILE-'olv-vagrant'}

# region to make the api calls to, relates to where the server is
# and the keypair's available in that region
AWS_REGION=${AWS_REGION-'us-west-1'}

# Name to show in the AWS Console pages
INSTANCE_NAME=${INSTANCE_NAME-'olv.encoder'}

# Type         vCPU     Mem (GiB)
# c3.large       2       3.75
# c3.xlarge      4       7.5
# c3.2xlarge     8      15
# c3.4xlarge    16      30
# c3.8xlarge    32      60
INSTANCE_TYPE=${INSTANCE_TYPE-'c3.large'}

# keypair installed for auth access
KEYPAIR_NAME=${KEYPAIR_NAME-'olv-encoder'}

echo ""
echo "API"
echo "    Profile:  $AWS_PROFILE"
echo "    Region:   $AWS_REGION"
echo ""
echo "Creating an instance"
echo "    Region:   $AWS_REGION"
echo "    Name:     $INSTANCE_NAME"
echo "    Type:     $INSTANCE_TYPE"
echo "    Key Pair: $KEYPAIR_NAME"

# Create the instance, and store the ID in a variable
INSTANCE=$(aws ec2 run-instances \
    --profile $AWS_PROFILE --region $AWS_REGION \
    --image-id ami-a15107c1 \
    --security-groups vagrant-vms \
    --count 1 \
    --instance-type $INSTANCE_TYPE \
    --key-name $KEYPAIR_NAME \
    --query 'Instances[0].InstanceId' --output text)

echo ""
echo "Finished creating instance"
echo "    Instance ID: $INSTANCE"

aws ec2 create-tags --profile 'olv-vagrant' --region 'us-west-1' --tags Key=Name,Value=$INSTANCE_NAME --resource $INSTANCE

echo ""
echo "Finished tagging instance"
echo "    Instance Name: $INSTANCE_NAME"

echo ""
echo "Waiting for instance to start up..."

# this is essentially a replacement for multiple API calls in a for loop
# our goal is to wait for the PublicDnsName to be available, and in testing this wait for 'instance-running'
# function in the awscli does the trick
aws ec2 wait instance-running --profile $AWS_PROFILE --region $AWS_REGION --instance-ids $INSTANCE

echo "    ... Looks like the instance is running now."

INSTANCE_GET_DNS_COMMAND="aws ec2 describe-instances --profile $AWS_PROFILE --region $AWS_REGION --query 'Reservations[*].Instances[0].PublicDnsName' --output text --instance-ids $INSTANCE"

echo ""
echo "Fetching the the PublicDnsName with the following command..."
echo "    (it takes some seconds for the network to be setup for the instance)"
echo ""
echo "    $INSTANCE_GET_DNS_COMMAND"
echo ""

INSTANCE_DNS=$(eval $INSTANCE_GET_DNS_COMMAND)
echo "    PublicDnsName: $INSTANCE_DNS"


echo ""
echo "Instance id: $INSTANCE"
echo "Server DNS: $INSTANCE_DNS"
echo ""

CMD_UPLOAD="bin/ec2-upload-files.sh $INSTANCE_DNS"
CMD_PROVISION="ssh -i config/olv-encoder.pem ubuntu@$INSTANCE_DNS 'cd ~/encode && bin/provision-ubuntu-14.04.sh'"

echo "We will now provision the server... "
echo ""
echo "    $CMD_UPLOAD"
echo "    $CMD_PROVISION"
echo ""
echo "    ... provisioned."
echo ""
echo "Running the upload command..."
echo ""
eval $CMD_UPLOAD
echo ""
echo "    ... done uploading."
echo ""
echo "Running the provision command..."
echo ""
eval $CMD_PROVISION
echo ""
echo "    ... done provisioning."
echo ""
echo "You may now ssh into the server with the following command..."
echo ""
echo "    ssh -i config/olv-encoder.pem ubuntu@$INSTANCE_DNS -t tmux"

