TL;DR
=====

    bash script/build-dist.sh

Overview
========


App Dependencies
================

At the top level we have a package.json, for Node.JS. This is used to drive build
scripts written in Gulp.

In the sub-directory ```./app-main``` is with another package.json, which
is used for only NPM packages which should be included in the distribution
of the app. Including build tools here will result in shipping those tools
with our app the the end-user.

Video Encoder
=============

There is a Vagrant driven virtual machine in the sub-directory ```./encode-vm```
(This may be moved to it's own project soon, but the JSON that drives the app is
mostly the same between the two projects.)

Setup Dev
=========

It's requred to use an Mac machine to build releases for both Mac and Windows,
You could run this from windows - however it seems overkill to setup two machines
unless there is trouble with getting wine on the Mac.

It's recommended to use "nodeenv" project to manage and issolate your Node.JS env.

    # create a node.js env with this specific version
    nodeenv --node=5.10.1 --prebuilt env

    # activate it this way
    # (you'll notice the 'env' in your command line prompt indicating it's active)
    . ./env/bin/activate

    # with node available, run the following to fetch vendor files
    npm install

    # disable nodeenv in the current session
    ./env//

---

Signing notes
=============

Code signing the Mac App file as well as the Windows EXE file are required in order
to avoid warnings to users about unidentified developers. This is likely the most
obtuse and complex part of the build, since over time this process changes.

The goal is to attemp and automate most of it, but the main hurdles are familiarizing
yourself with the overall process.


Settings and Versioning
=======================

Each year we need to build a new version versions of the offline lecture USBs for shipment to students. Both full and cram.
 2 new settings files will be created on each iteration:

- **settings-full-[BUNDLE_VERSION].json**
- **settings-cram-[BUNDLE_VERSION].json**

They will send us a single data.json containing all the **cram** and **full** version videos.
The steps to split are:

1.  Create 2 files with the contents of data.json:
    1. settings-full-[BUNDLE\_VERSION].json
    2. settings-cram-[BUNDLE\_VERSION].json

2. Edit the settings-full-[BUNDLE\_VERSION].json
    1. remove the courses categories that start with:
AUD-CRAM , BEC-CRAM, FAR-CRAM, REG-CRAM
    2. SETUP specific variables at the json root
         ```
          "user_data_dir": "OfflineLectures-full-[BUNDLE_VERSION]",
              "auth_extra_args": {
                  "app_bundle_id": "rcpar.courseware-v2.1.2017"
              }
          }
          ```

3. Edit the settings-cram-[BUNDLE\_VERSION].json

    1.  Remove the courses categories : AUD , BEC, FAR, REG
    2.  SETUP specific variables at the json root
        ```
              "user_data_dir": "OfflineLectures-cram-2017",
                "auth_extra_args": {
                    "app_bundle_id": "rcpar.courseware-v2.1.2017"
                },
              "display_chapters": false,
        ```

    3.  (DEPRICATED) On each course, add the following line to share the same theme (colors, etc) so  for example BEC-CRAM looks equal to BEC course:
        ```
           "AUD-CRAM": {
               "theme": "AUD",
               "chapters": {...}
           ...
           "BEC-CRAM": {
               "theme": "BEC",
               "chapters": {...}
           ...
           "FAR-CRAM": {
               "theme": "FAR",
               "chapters": {...}
           ...
           "REG-CRAM": {
               "theme": "REG",
               "chapters": {...}
        ```

Build Versions example
======================

### Full 2016
    
    BUNDLE_VERSION='2016' \
        APP_MODE='full' \
        DO_SIGN_MAC_BUILD=yes \
        SERVICE_AUTH_URL='https://www.rogercpareview.com/user/ssologin/offline-lec' \
        ./bin/build-dist.sh
    
### Cram 2016

    BUNDLE_VERSION='2016' \
        APP_MODE='cram' \
        DO_SIGN_MAC_BUILD=yes \
        SERVICE_AUTH_URL='https://www.rogercpareview.com/user/ssologin/offline-lec' \
        ./bin/build-dist.sh
    
### Full 2017

    BUNDLE_VERSION='2017' \
        APP_MODE='full' \
        DO_SIGN_MAC_BUILD=yes \
        SERVICE_AUTH_URL='https://www.rogercpareview.com/user/ssologin/offline-lec' \
        ./bin/build-dist.sh
    
### Cram 2017

    BUNDLE_VERSION='2017' \
        APP_MODE='cram' \
        DO_SIGN_MAC_BUILD=yes \
        SERVICE_AUTH_URL='https://www.rogercpareview.com/user/ssologin/offline-lec' \
        ./bin/build-dist.sh

---

Release (Manually)
==================

Results in 4 zips, one for each platform/version.

Meant to run on a dev machine that has access to sign identities for prod release.

    ./bin/release-manual.sh

---

### mac-osx env notes

- http://www.kylethielk.com/blog/build-ios-app-with-jenkins-code-signing-issues/
- https://github.com/nwjs/nw.js/wiki/MAS%3A-Requesting-certificates
- http://www.nuxeo.com/blog/sign-application-windows-os/

### windows env notes

- not currently tested on windows
