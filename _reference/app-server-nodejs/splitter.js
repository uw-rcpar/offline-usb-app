var fs = require('fs'),
    path = require('path'),
    argv = require('minimist')(process.argv.slice(2));

// get the command for the script
var command = argv._.pop();

var encoding = 'binary';

// console.dir(argv);
// console.dir(command);

// based on the given command, run the correct fucntion
switch (command) {
    case 'split':
        if (!('file' in argv)) {
            throw new Error("split command requires the --file option.");
        }

        split(argv.file);
        break;

    case 'combine':
        if (!('src' in argv)) {
            throw new Error("combine command requires the --src option.");
        }
        combine(argv.src);
        break;
}

/**
 * Combile a file into one part
 */
function combine(src) {

    // var src = path.join('chunks', dir_in);
    var dest = path.join('combined', path.basename(src));

    // console.dir('Combining the files from ' + src + ' and saving under ' + dest);

    // parent dir needs to exist first
    // we create the dest where the chunks will be stored
    // fs.mkdirSync(dest, '0644');
    mkdirRecursive(path.dirname(dest));

    // get the total filesize of the input file

    // fs.readSync(file, buffer, offset, length, position);

    var files = fs.readdirSync(src);

    // console.log('Counting files', files);

    // open a file buffer for writing
    // console.log('opening file: ', dest);
    var write_out = path.join(dest);
        fd_write = fs.openSync(write_out, 'w');
    var file_count = files.length;
    for (var i = 0; i < file_count; i++) {
        var file_in = path.join(src, files[i]);
        // console.log('reading file: ', file_in);
        var buffer = fs.readFileSync(file_in);
        // write buffer to file
        // console.log('writing to file: ', write_out);
        fs.writeFileSync(fd_write, buffer, {flag: 'a'});
    }

    // done writing
    // console.log('closing file: ', write_out);
    fs.closeSync(fd_write);

    var info = fs.statSync(write_out);
    console.log(info);

    // // open a file buffer for writing
    // console.log('opening file: ', dest);
    // var file_out = path.join(dest);
    //     write_stream = fs.createWriteStream(file_out);
    // var file_count = files.length;
    // for (var i = 0; i < file_count; i++) {
    //     var file_in = path.join(src, files[i]);
    //     console.log('reading file: ', file_in);
    //     var buffer = fs.readFileSync(file_in);
    //     // write buffer to file
    //     console.log('writing to file: ', file_out);
    //     write_stream.write(buffer);
    // }
    // // done writing
    // console.log('closing file: ', file_out);
    // write_stream.end();

    return;
}

/**
 * Split a file into multiple parts
 */
function split(file_in) {

    var dest = path.join('chunks', file_in);

    // console.dir('splitting the file ' + file_in + ' and saving under ' + dest);

    // parent dir needs to exist first
    // we create the dest where the chunks will be stored
    // fs.mkdirSync(dest, '0644');
    mkdirRecursive(dest);

    // get the total filesize of the input file

    // fs.readSync(file, buffer, offset, length, position);

    // console.log('Reading file_in', file_in);

    var info = fs.statSync(file_in),
        file_length = info.size;

    // console.log(info);
    // console.log(file_length);

    fs.open(file_in, 'r', function(status, fd_read){
        if (status) {
            // console.log(status.message);
            return;
        }

        // console.log('file descriptor: ', fd_read);

        // read file to buffer
        var buffer_size = 1024 * 1024 * 10; // 10mb chunks
        // var buffer_size = 3;
        // get the length of the file, and calculate how many times we need to chunk
        var total_chunks = Math.ceil(file_length / buffer_size);
        // track how many chunks we've created
        var chunk_count = 0;

        // console.log('total_chunks', total_chunks);

        // while we have chunks to chunk
        while (chunk_count < total_chunks) {

            // position to start reading from
            var position = buffer_size * chunk_count; // position is where we start reading the file from

            // check to make sure the buffer isn't larger than the remaining length of the file, if it is we
            // need to make the buffer size smaller, so we don't have junk at the end of the last file
            // note: only need to do this on the last pass
            if (chunk_count + 1 == total_chunks) {
                var extra_size = (total_chunks * buffer_size) - file_length;
                buffer_size = buffer_size - extra_size;
            } else {
                var extra_size = null;
            }

            var buffer = new Buffer(buffer_size), // store the chunk of the file in the buffer
                offset = 0, // always write to the first part of the buffer
                length = buffer_size; // length is how much of the file to read, should match what we can fit in the buffer

            // read the chunk of the file
            var bytes_read = fs.readSync(fd_read, buffer, offset, length, position);

            // console.log({
            //     bytes_read: bytes_read,
            //     buffer_size: buffer_size,
            //     extra_size: extra_size || null,
            //     position: position
            // });

            // append the count of the file parts to the end, pad with 0's
            chunk_str = chunk_count + '';
            var chunk_pad = '0000000000';
            var chunk_str = chunk_pad.substring(0, chunk_pad.length - chunk_str.length) + chunk_str + '.chunk';

            // console.log('output: ', dest, chunk_count, write_out);
            // var write_to = path.join(dest, chunk_count);

            // we can write the file chunk here
            // console.log('read chunk of the file', bytes_read);
            // console.log('writing out chunk ' + chunk_count + ' to file ' + file_out);

            // try with fs.createWriteStream
            //
            // var wstream = fs.createWriteStream(path.join(dest, chunk_str));
            // wstream.write(buffer);
            // wstream.end();

            // try with fs.writeSync || fs.writeFileSync
            //
            var write_out = path.join(dest, chunk_str)
                fd_write = fs.openSync(write_out, 'w');
            fs.writeFileSync(fd_write, buffer);
            // fs.writeSync(fd_write, buffer, 0, buffer.length);
            fs.closeSync(fd_write);

            // increment so we finally exit when all chunks have been read
            chunk_count++;
        }

        // // write a new file here, sync

        // // increment the chunk count
        // chunk_count++;
        // // increment the next position to start at
        // position = chunk_count * buffer_size;

        // while (var bytes_read = fs.readSync(fd, buffer, 0, buffer_size, 0, function(err, num) {
        //     console.log(buffer.toString('utf8', 0, num));
        // });
    });

    // console.log('Writing file_out');
    // fs.writeSync(file_out, buffer);

    // // converts blob to base64
    // var blobToBase64 = function(blob, cb) {
    //     var reader = new FileReader();
    //     reader.onload = function() {
    //         var dataUrl = reader.result;
    //         var base64 = dataUrl.split(',')[1];
    //         cb(base64);
    //     };
    //     reader.readAsDataURL(blob);
    // };

    // // encode
    // blobToBase64(blob, function(base64) {
    //     var update = {'blob': base64};
    //     $http.post('/api/save_recording', update)
    //         .success(function(new_recording) {
    //             console.log("success");
    //         }
    //     })
    // })
}

function mkdirRecursive(dir_string) {
    var dir_array = dir_string.split('/'),
        paths = [],
        dir_path;

    for (var i = 0; i < dir_array.length; i++) {
        paths.push(dir_array[i]);
        dir_path = paths.join('/');
        if (!dirExists(dir_path)) {
            // console.dir('dir_path ' + dir_path + ' NOT exists');
            fs.mkdirSync(dir_path);
        } else {
            // console.dir('dir_path ' + dir_path + ' DOES exist');
        }

    }
}

function dirExists(path)
{
    try {
        return fs.statSync(path).isDirectory();
    } catch (err) {
        return false;
    }
}
