var express = require('express'),
    app = express(),
    path = require('path')
    fs = require('fs');

var setting_chunks_path = 'chunks/videos';
var setting_key_path = 'keys';
var setting_key_file = 'roger-20160505.key';
var stub_user = 'user';
var stub_pass = 'pass';
var stub_videos = [
    { key: 1, file: 'FAR-0.01.webm', title: 'FAR-0.01 (webm)' },
    { key: 2, file: 'FAR-0.01.enc.webm', title: 'FAR-0.01 (webm encrypted)' },
    // { key: 1, file: 'FAR-0.01.mp4', title: 'FAR-0.01' },
    // { key: 1, file: 'FAR-0.01.webm', title: 'FAR-0.01 (webm)' },
    // { key: 2, file: 'FAR-1.05.mp4', title: 'FAR-1.05' },
    // { key: 3, file: 'FAR-11.04.mp4', title: 'FAR-11.04' },
    // { key: 4, file: 'SampleVideo_1280x720_1mb.mp4', title: 'Sample 1mb' },
    // { key: 5, file: 'SampleVideo_1280x720_2mb.mp4', title: 'Sample 2mb' },
    // { key: 6, file: 'SampleVideo_1280x720_5mb.mp4', title: 'Sample 5mb' },
    { key: 3, file: 'HTML5-WEBM-2MB.webm', title: 'Sample 2mb (webm)' },
    { key: 4, file: '001-HTML5-WEBM-2MB.webm', title: 'Test 2mb (webm encrypted)' }
];

/**
 * Find a single video in the array of videos
 */
function searchVideosByKey(key, videos) {
    for (var i = 0; i < videos.length; i++) {
        // console.log('checking video looking for key ' + key, videos[i]);
        // console.log('videos[i].key', videos[i].key);
        // console.log('key', key);
        if (parseInt(videos[i].key) === parseInt(key)) {
            return videos[i];
        }
    }

    return false;
}

app.get('/', function (req, res) {
    res.send('Video Checkout System');
});

app.get('/fetch-license', function (req, res) {
    // find video in the list
    // list parts
    var key_path = path.join(setting_key_path, setting_key_file);
    // get path to the chunk of the file
    var path_to_chunk = path.resolve(key_path);
    // return the key
    res.sendFile(path_to_chunk);
});

app.get('/directory', function (req, res) {
    var directory = {
        videos: stub_videos
    };

    // create filename and url keys
    // create filename and url keys
    for (var i = 0; i < directory.videos.length; i++) {
        try {
            // list parts
            var parts = fs.readdirSync(path.join(setting_chunks_path, directory.videos[i].file));
            directory.videos[i].parts_count = parts.length;
        } catch (e) {
            directory.videos[i].parts_count = false;
        }
    }

    res.json(directory);
});

app.get('/checkout/:key', function (req, res) {
    // find video in the list
    var video = searchVideosByKey(req.params.key, stub_videos);

    // list parts
    var parts = fs.readdirSync(path.join(setting_chunks_path, video.file));

    //  build full url so we could even use a cdn if we wanted
    var full_url = req.protocol + '://' + req.get('host') + req.originalUrl;

    // create filename and url keys
    for (var i = 0; i < parts.length; i++) {
        parts[i] = { filename: parts[i], url: full_url + '/' + parts[i] };
    }

    // add data into the parts array
    video.parts_count = parts.length;
    video.parts = parts;

    console.log(video);

    res.json(video);
});

app.get('/checkout/:key/:part', function (req, res) {
    // find video in the list
    var video = searchVideosByKey(req.params.key, stub_videos);
    // list parts
    // var parts = fs.readdirSync(path.join(setting_chunks_path, video.file));
    // get path to the chunk of the file
    var path_to_chunk = path.resolve(path.join(setting_chunks_path, video.file, req.params.part));
    // return the chunk
    res.sendFile(path_to_chunk);
});

app.listen(3000, function() {
    console.log('Video Checkout System listening on port 3000');
});
