Overview
========

- Provides a splitter / combine script (splitter.js)
- Service split files to client apps (server.js)

TL;DR
=====

    npm install && node server.js

Setup
=====

    # if using nodeenv to install nodejs
    nodeenv --node=5.10.1 --prebuilt env
    . ./env/bin/activate

    # with node available, run the following to fetch vendor files
    npm install

Run server
==========

    node server.js

