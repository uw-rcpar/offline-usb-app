#!/bin/bash

# mac app store
# ./node_modules/.bin/electron-packager . --arch=x64 --platform=mas --overwrite --out ./dist

# all, in an asar archive
./node_modules/.bin/electron-packager . --asar --arch=all --platform=all --overwrite --out ./dist
