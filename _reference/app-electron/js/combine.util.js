var fs = require('fs'),
    path = require('path');

/**
 * Combile a file into one part
 */
function combine(src) {

    // var src = path.join('chunks', dir_in);
    var dest = path.join('combined', path.basename(src));

    // console.dir('Combining the files from ' + src + ' and saving under ' + dest);

    // parent dir needs to exist first
    // we create the dest where the chunks will be stored
    // fs.mkdirSync(dest, '0644');
    mkdirRecursive(path.dirname(dest));

    // get the total filesize of the input file

    // fs.readSync(file, buffer, offset, length, position);

    var files = fs.readdirSync(src);

    console.log('Counting files', files);

    // open a file buffer for writing
    // console.log('opening file: ', dest);
    var write_out = path.join(dest);
        fd_write = fs.openSync(write_out, 'w');

    var file_count = files.length;

    // console.log('Write out', write_out);

    for (var i = 0; i < file_count; i++) {
        var file_in = path.join(src, files[i]);
        // console.log('reading file: ', file_in);
        var buffer = fs.readFileSync(file_in);
        // write buffer to file
        // console.log('writing to file: ', write_out);
        fs.writeFileSync(fd_write, buffer, {flag: 'a'});
    }

    // done writing
    // console.log('closing file: ', write_out);
    fs.closeSync(fd_write);

    var info = fs.statSync(write_out);

    console.log(info);

    // // open a file buffer for writing
    // console.log('opening file: ', dest);
    // var file_out = path.join(dest);
    //     write_stream = fs.createWriteStream(file_out);
    // var file_count = files.length;
    // for (var i = 0; i < file_count; i++) {
    //     var file_in = path.join(src, files[i]);
    //     console.log('reading file: ', file_in);
    //     var buffer = fs.readFileSync(file_in);
    //     // write buffer to file
    //     console.log('writing to file: ', file_out);
    //     write_stream.write(buffer);
    // }
    // // done writing
    // console.log('closing file: ', file_out);
    // write_stream.end();

    return write_out;
}

function mkdirRecursive(dir_string) {
    var dir_array = dir_string.split('/'),
        paths = [],
        dir_path;

    for (var i = 0; i < dir_array.length; i++) {
        paths.push(dir_array[i]);
        dir_path = paths.join('/');
        if (!dirExists(dir_path)) {
            // console.dir('dir_path ' + dir_path + ' NOT exists');
            fs.mkdirSync(dir_path);
        } else {
            // console.dir('dir_path ' + dir_path + ' DOES exist');
        }

    }
}

function dirExists(path)
{
    try {
        return fs.statSync(path).isDirectory();
    } catch (err) {
        return false;
    }
}
