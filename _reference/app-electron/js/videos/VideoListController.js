angular.module('landing', ['ngMaterial', 'ngElectron'])

// // html5 video fix
// .directive('html5vfix', function() {
//     return {
//         restrict: 'A',
//         link: function(scope, element, attr) {
//             attr.$set('src', attr.vsrc);
//         }
//     }
// })

.controller('VideoListController', ['$scope', '$http', 'electron', function($scope, $http, electron) {

    // indexdb global vars
    var db,
        dbName = "rcpar-proof",
        dbVersion = 10,
        openIndexDB = indexedDB.open(dbName, dbVersion);

    // general utilities and vars
    var fs = electron.fs,
        path = electron.path,
        app = electron.app,
        data_path = path.join(app.getPath('userData'), 'roger-data', 'videos'),
        directory_url = 'http://chris.strsocial.com:3000/directory',
        download_url = 'http://chris.strsocial.com:3000/checkout/', // support key for overall info and key/filename for chunks
        license_url = 'http://chris.strsocial.com:3000/fetch-license',
        fsExtra = require('fs-extra'),
        request = require('request');

    // initial scope states
    $scope.indexdb_is_available = false;
    $scope.data_path = data_path;
    $scope.did_init_encrypted_handler = false;
    // hide page till done loading
    // 0 == not loaded, 1 == loading, 2 == loaded
    $scope.is_loading = 0;
    // stores the list of videos
    $scope.directory = [];
    $scope.sources = [];

    //
    // indexdb initial
    //
    openIndexDB.onerror = function(event) {
      // Handle errors.
      console.log('error');
    };

    openIndexDB.onsuccess = function(event) {
        console.debug('openIndexDB.onsuccess');
        db = event.target.result;

        // init page only after indexdb available and loaded
        fetchLocalVideoList().then(function(){
            triggerViewSync();
            console.log('Complete!', $scope.directory);
            $scope.indexdb_is_available = true;
            $scope.is_loading = 2;
            // hacky fix to refresh the view layer in angular
            setInterval(function() {
                $scope.$apply()
            }, 500);
        });
    }

    // on every 'update' we just delete known object stores and re-init them
    openIndexDB.onupgradeneeded = function(event) {
        console.debug('openIndexDB.onupgradeneeded');
        var db = event.target.result;

        // Create an objectStore to hold information about our customers. We're
        // going to use "file" as our key path because it's guaranteed to be
        // unique like a file name
        var stores_to_delete = ['videos', 'video-keys', 'video-meta', 'video-keys'];

        for (var i = 0; i < stores_to_delete.length; i++) {
            try {
                db.deleteObjectStore(stores_to_delete[i]);
            } catch (e) {
                console.debug("Did not need or not able to delete " + stores_to_delete[i] + " db");
            }
        }

        // store meta about videos here
        var objectStoreMeta = db.createObjectStore("video-meta", { keyPath: "key" });
        // var objectStoreMeta = db.createObjectStore("video-meta");

        // store binary data of the videos here
        var objectStoreKeys = db.createObjectStore("video-keys");

        // Create an index to search customers by name. We may have duplicates
        // so we can't use a unique index.
        // objectStore.createIndex("name", "name", { unique: false });

        // Create an index to search customers by email. We want to ensure that
        // no two customers have the same email, so use a unique index.
        // objectStore.createIndex("email", "email", { unique: true });

        var generalOnComplete = function(event) {
            console.debug('objectStore.transaction.oncomplete');
        };

        // Use transaction oncomplete to make sure the objectStore creation is
        // finished before adding data into it.
        objectStoreMeta.transaction.oncomplete = generalOnComplete;
        objectStoreKeys.transaction.oncomplete = generalOnComplete;
    };

    /**
     * handle fetch from local indexdb
     */
    var fetchLocalVideoList = function() {

        // reset local directory
        $scope.directory = [];

        // create a promise to wrap our db call within
        var promise = new Promise(function(resolve, reject) {

            // setup call to db
            var transaction = db.transaction(["video-meta"], "readwrite");
                object_store = transaction.objectStore("video-meta"),
                cursor_request = object_store.openCursor();

            // Using a normal cursor to grab whole customer record objects
            cursor_request.onsuccess = function(event) {
                var cursor = event.target.result;
                if (cursor) {
                    // add the db value to the
                    $scope.directory.push(cursor.value);

                    console.debug(cursor.key, cursor.value);

                    cursor.continue();
                } else {
                    // if we have no more cursor results, we end,
                    // and we resolve promise
                    console.debug('Done with video-meta cursor.');
                    resolve();
                }
            };
        });

        return promise;
    }

    /**
     * loaad videos from the remote server directory listing
     */
    var fetchRemoteVideoList = function() {

        var new_directory = [];

        // create a promise to wrap our db call within
        var promise = new Promise(function(resolve, reject) {

            $http.get(directory_url).then(function(res){
                // check status of each video 'downlaoded' status
                // push updated video objects to the $scope.directory
                angular.forEach(res.data.videos, function(server_video, i){
                    console.log('server_video', server_video, i);

                    // search existing directory for a video with the same key,
                    // IMPORTANT: we are assuming here that $scope.directory is
                    // a list updated from IndexDB
                    var found = false;
                    angular.forEach($scope.directory, function(local_video, j){
                        // look for video with the same key
                        if (server_video.key == local_video.key) {
                            found = true;
                            console.log('Key Found! Updating local_video with server_video');
                            var original_video = $scope.directory[j];
                            $scope.directory[j] = server_video;
                            $scope.directory[j].is_downloaded = original_video.is_downloaded;
                            $scope.directory[j].parts_downloaded = original_video.parts_downloaded;
                            $scope.directory[j].is_downloading = original_video.is_downloading;
                            updateDatabaseEntryForVideo($scope.directory[j]);
                        }
                    });

                    // enter new videos into the scope directory here
                    if (!found) {
                        console.log('Key NOT FOUND! Creating new local_video entry from server_video');
                        var new_video = server_video;
                        new_video.is_downloaded = false;
                        new_video.parts_downloaded = 0;
                        new_video.is_downloading = false;
                        $scope.directory.push(new_video);
                        updateDatabaseEntryForVideo(new_video);
                    }
                });

                // resolve the parent promise
                console.debug('Resolving remote server fetch from fetchRemoteVideoList()');
                resolve();

                console.log('Updated directory, writing to IndexDB', $scope.directory);
            });
        });

        return promise;
    }

    /**
     * Update one entry
     */
    var updateDatabaseEntryForVideo = function(video) {

        var promise = new Promise(function(resolve, reject){
            var transaction = db.transaction(["video-meta"], "readwrite"),
                object_store = transaction.objectStore("video-meta"),
                db_request = object_store.put(video);

            console.log('Video updating...', video.key);
            db_request.onsuccess = function(event) {
                resolve();
            };

            db_request.onerror = function(event) {
                console.log('video NOT found in indexdb!', video);
                reject();
            }
        });

        return promise;

    }

    /**
     * Check IndexDB and FileSystem to verify status of the video
     */
    var triggerViewSync = function() {

        var updated_directory = [];

        // check status of each video 'downlaoded' status
        // push updated video objects to the $scope.directory
        angular.forEach($scope.directory, function(video, key){
            video.is_downloaded = $scope.isDownloaded(video);
            video.is_downloading = 0;
            video.download_progress = 100;
            this.push(video);

            // write to indexdb

        }, updated_directory);

        console.log('updating $scope.directory');
        $scope.directory = updated_directory;

        // hacky fix to refresh the view
        setInterval(function() {
            $scope.$apply()
        }, 500);

        updated_directory = null;
    }

    var getVideoLocalPath = function(video) {
        var local_path = path.join(data_path, video.file);
        return local_path;
    }

    var dirExists = function(path) {
        try {
            // console.log('checking path', path);
            return fs.statSync(path).isDirectory();
        } catch (e) {
            return false;
        }
    }

    var fileExists = function(path) {
        try {
            // console.log('checking path', path);
            return fs.statSync(path).isFile();
        } catch (e) {
            return false;
        }
    }

    var partsInDir = function(path) {
        var stats = fs.readdirSync(path);
        return stats;
    }

    var putLicenseInDb = function(blob) {
        console.log("Putting license in IndexedDB");

        // Open a transaction to the database
        var transaction = db.transaction(["video-keys"], "readwrite");

        // Put the blob into the dabase
        // '12345' could be the content_id for the key - but for now we're just using 12345
        var key_content_id = '12345';
        var put = transaction.objectStore("video-keys").put(blob, key_content_id);

        put.onsuccess = function(event){
            console.log('Put license key success, loading to global KEY array');
            $scope.has_license = true;
            loadKeyFromDatabase();
        };
    }

    // store a list of keys for our videos
    var KEY = null;

    var loadKeyFromDatabase = function() {
        console.log('loadKeyFromDatabase');
        doesLicenseKeyExist("12345").then(function(exists, blob){
            var blob = event.target.result;
            console.log('result', blob);
            if (exists) {
                // found
                console.log('converting from blob to uint8array');
                var uint8ArrayNew  = null;
                var arrayBufferNew = null;
                var fileReader     = new FileReader();
                fileReader.onload  = function(progressEvent) {
                    arrayBufferNew = this.result;
                    uint8ArrayNew  = new Uint8Array(arrayBufferNew);
                    KEY = uint8ArrayNew;
                    console.log('Video license should be ready to try', uint8ArrayNew);
                };
                fileReader.readAsArrayBuffer(blob);
            }
        });
    }

    $scope.updateDownloadProgress = function(video) {
        console.log(video);
    }

    $scope.deleteVideo = function(video) {
        var delete_path = getVideoLocalPath(video);
        if (delete_path != data_path) {
            console.log('Path to delete', delete_path);
            fsExtra.emptydirSync(delete_path);
            triggerViewSync();
        }
    }

    $scope.onClickFetchList = function() {
        fetchRemoteVideoList().then(function(){
            // update loading status when done
            $scope.is_loading = 2;
            triggerViewSync();
        });
    }

    $scope.isDownloaded = function(video) {
        var is_downloaded = false;
        var local_path = getVideoLocalPath(video),
            video_parts;

        if (!dirExists(local_path)) {
            // console.log('video local path does not exist, so it\'s not downloaded.');
            is_downloaded = false;
        } else {
            var video_parts = partsInDir(local_path);
            if (video_parts.length == parseInt(video.parts_count)) {
                is_downloaded = true;
            } else {
                is_downloaded = false;
            }
        }

        return is_downloaded;
    }

    /**
     * Get a key, again, we assume 1 key in this example
     */
    var doesLicenseKeyExist = function(content_id) {
        console.log('doesLicenseKeyExist', db);

        var promise = new Promise(function(resolve, reject){
            var transaction = db.transaction(["video-keys"], "readwrite");
            var object_store = transaction.objectStore("video-keys");
            var db_request = object_store.get(content_id);
            console.log(db_request);
            db_request.onsuccess = function(event){
                var blob = event.target.result;

                if (typeof blob == 'undefined') {
                    resolve(false, null);
                } else {
                    resolve(true, blob);
                }
            };
        });

        return promise;
    }

    /**
     * Check for a license, assume we only have 1 for now
     */
    $scope.downloadLicenseKey = function() {
        doesLicenseKeyExist("12345").then(function(exists, blob) {
            if (!exists) {
                console.log('No key found, fetching...');
                $scope.onRequestLicense();
            }
        });
    }

    /**
     * Delete key from the database
     */
    $scope.deleteLicenseKey = function() {
        console.log('doesLicenseKeyExist', db);
        var transaction = db.transaction(["video-keys"], "readwrite");
        var object_store = transaction.objectStore("video-keys");
        var request = object_store.delete('12345');
        request.onsuccess = function(event) {
            $scope.has_license = false;
        };
    }

    $scope.downloadVideo = function(video) {
        console.log('downloadVideo', video);

        video.is_downloading = true;
        video.download_progress = 0;

        var url = download_url + video.key,
            parts = parseInt(video.parts_count), // each part is an object with filename and url
            storage_dir = getVideoLocalPath(video),
            chunks_to_download = [],
            chunks_total = 0,
            chunks_downloaded = 0;

        var updateVideoProgress = function() {
            try {
                console.log({
                    chunks_downloaded: chunks_downloaded,
                    chunks_total: chunks_total,
                    download_progress: (chunks_downloaded / chunks_total) * 100
                });

                video.download_progress = (chunks_downloaded / chunks_total) * 100;
            } catch (e) {
                console.log('error thrown', chunks_downloaded, chunks_total);
                // catch division by zero, edge case but maybe possible
                video.download_progress = 0;
            }

            if (video.download_progress >= 100) {
                console.log('trigger view sync');
                video.is_downloading = false;
                video.download_progress = 100;
                triggerViewSync();
            }

            console.log('progress: ', video.download_progress);
        }

        var downloadNextChunk = function() {

            if (chunks_to_download.length == 0) {
                console.log('All chunks downloaded');
                updateVideoProgress();
                return;
            } else {
                console.log('chunks remaining', chunks_to_download.length);
                updateVideoProgress();
            }

            // get the next chunk
            var chunk = chunks_to_download.pop();

            // var fs = require('fs');
            var stream = request(chunk.url),
                file_path = path.join(storage_dir, chunk.filename),
                writeStream = fs.createWriteStream(file_path);

            stream.on('data', function(data) {
                // console.log('write stram data');
                writeStream.write(data);
            });

            stream.on('end', function() {
                console.log('Saved chunk to ' + chunk.filename);
                chunks_downloaded++;
                downloadNextChunk();
                writeStream.end();
            });

            stream.on('error', function(err) {
                console.log('something is wrong :( ');
                writeStream.close();
            });
        }

        // get video details
        var startVideoDownload = function(url, video) {
            $http.get(url).then(function(res){
                // check status of each video 'downlaoded' status
                // push updated video objects to the $scope.directory
                angular.forEach(res.data.parts, function(chunk, key){
                    console.log('Checking if chunk exists: ', path.join(storage_dir, chunk.filename));
                    chunks_total++;
                    if (!fileExists(path.join(storage_dir, chunk.filename))) {
                        // download the chunk, and write the result to a file
                        chunks_to_download.push(chunk);
                    } else {
                        chunks_downloaded++;
                    }

                    updateVideoProgress(video);
                });

                console.log('Chunks that need to be downloaded', chunks_to_download);

                downloadNextChunk(video);

                // // @see http://stackoverflow.com/questions/31581254/how-to-write-a-file-from-an-arraybuffer-in-js
                // $http.get(chunk.url, {responseType: 'arraybuffer'}).then(function(res){
                //     var file_path = path.join(storage_dir, chunk.filename);
                //     var unit_8_array = new Uint8Array(res.data);
                //     fs.writeFileSync(file_path, new Buffer(unit_8_array));
                //
                //     triggerViewSync();
                // });
            });
        }

        startVideoDownload(url, video);

        // make sure we have a directory to check
        if (!dirExists(storage_dir)) {
            fsExtra.mkdirsSync(storage_dir);
        }

        return;
    }

    /**
     * Listen for an event that fires when licenses are needed
     */
    var handleEncrypted = function(event) {
        console.log('handleEncrypted!', event);
        var session = event.target.mediaKeys.createSession();
        session.addEventListener('message', handleMessage, false);
        session.generateRequest(event.initDataType, event.initData).catch(
            function(error) {
                console.log('Failed to generate a license request', error);
            }
        );
    }

    var handleMessage = function(event) {
        // If you had a license server, you would make an asynchronous XMLHttpRequest
        // with event.message as the body.  The response from the server, as a
        // Uint8Array, would then be passed to session.update().
        // Instead, we will generate the license synchronously on the client, using
        // the hard-coded KEY at the top.
        var license = generateLicense(event.message);

        var session = event.target;
        session.update(license).catch(
            function(error) {
                console.error('Failed to update the session', error);
            }
        );
    }

    /**
     * Convert Uint8Array into base64 using base64url alphabet, without padding.
     */
    var toBase64 = function(u8arr) {
      return btoa(String.fromCharCode.apply(null, u8arr)).
          replace(/\+/g, '-').replace(/\//g, '_').replace(/=*$/, '');
    }

    // This takes the place of a license server.
    // kids is an array of base64-encoded key IDs
    // keys is an array of base64-encoded keys
    function generateLicense(message) {
        // Parse the clearkey license request.
        var request = JSON.parse(new TextDecoder().decode(message));
        // We only know one key, so there should only be one key ID.
        // A real license server could easily serve multiple keys.
        console.assert(request.kids.length === 1);

        var keyObj = {
            kty: 'oct',
            alg: 'A128KW',
            kid: request.kids[0],
            k: toBase64(KEY)
        };

        return new TextEncoder().encode(JSON.stringify({
            keys: [keyObj]
        }));
    }

    $scope.onRequestLicense = function() {

        // Create XHR
         var xhr = new XMLHttpRequest(),
             blob;

         xhr.open("GET", license_url, true);

         // Set the responseType to blob
         xhr.responseType = "blob";

         xhr.addEventListener("load", function () {
             if (xhr.status === 200) {
                 console.log("License retrieved");

                 // Blob as response
                 blob = xhr.response;
                 console.log("Blob:" + blob);

                 // Put the received blob into IndexedDB
                 putLicenseInDb(blob);
             }
         }, false);
         // Send XHR
         xhr.send();
    }

    $scope.onDeleteLicense = function() {
    }

    $scope.playVideo = function(video) {
        console.log('playVideo');

        var url = download_url + video.key,
            storage_dir = getVideoLocalPath(video),
            parts = partsInDir(storage_dir),
            buffer_parts = [];

        angular.forEach(parts, function(part, key){
            var file_path = path.join(storage_dir, part);
                // info = fs.statSync(file_path);

            var buffer_part = fs.readFileSync(file_path);
            buffer_parts.push(buffer_part);
            buffer_part = null;
            // console.log('size', info.size);

            // var buffer_part = new Buffer(info.size);
            // var buffer_part = fs.readFileSync(file_path);
            // console.log('length', buffer_part.length);

            // var total_length = buffer_full.length + buffer_part.length;
            // buffer_full = Buffer.concat([buffer_full, buffer_part]);
        });

        console.log('combining ' + buffer_parts.length + ' buffers into one buffer');
        var buffer_full = Buffer.concat(buffer_parts);
        buffer_parts = null;

        // test write file_path
        // fs.writeFileSync(path.join(app.getPath('desktop'), 'test.mp4'), buffer_full);

        // console.log(buffer_full.length);
        // var data = buffer_full.toString('utf8');
        var data = new Blob( [ buffer_full ], {type: "video/mp4"} );
        var URL = window.URL || window.webkitURL;
        // console.log(URL.createObjectURL);
        var video_src = URL.createObjectURL(data);
        console.log('Setting video src to blob url', video_src);

        var video_el = document.querySelector('video');

        if (!$scope.did_init_encrypted_handler) {
            $scope.did_init_encrypted_handler = true;
            video_el.addEventListener('encrypted', handleEncrypted, false);
        }

        //
        // VIDEO DECODING
        //
        var config = [{
          initDataTypes: ['webm'],
          videoCapabilities: [{
            contentType: 'video/webm; codecs="vp8"'
          }]
        }];

        navigator.requestMediaKeySystemAccess('org.w3.clearkey', config).then(
            function(keySystemAccess) {
                return keySystemAccess.createMediaKeys();
            }
        ).then(
            function(createdMediaKeys) {
                console.log('returning video_el.setMediaKeys', createdMediaKeys);
                return video_el.setMediaKeys(createdMediaKeys);
            }
        ).catch(
            function(error) {
                console.error('Failed to set up MediaKeys', error);
            }
        );

        video_el.src = video_src;
        // $scope.sources = [{path: video_src, type: 'video/mp4'}];

        return;
    }
}]);
