angular.module('landing', ['ngMaterial', 'ngElectron'])

// html5 video fix
.directive('html5vfix', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {
            attr.$set('src', attr.vsrc);
        }
    }
})

.controller('VideoListController', ['$scope', '$http', 'electron', function($scope, $http, electron) {

    var db;
    var dbName = "rcpar-proof";
    var dbVersion = 6;
    var openIndexDB = indexedDB.open(dbName, dbVersion);

    openIndexDB.onerror = function(event) {
      // Handle errors.
      console.log('error');
    };

    openIndexDB.onsuccess = function(event) {
        db = event.target.result;
    }

    openIndexDB.onupgradeneeded = function(event) {
        var db = event.target.result;

        // Create an objectStore to hold information about our customers. We're
        // going to use "file" as our key path because it's guaranteed to be
        // unique like a file name
        // db.deleteObjectStore("videos");

        // store meta about videos here
        var objectStoreMeta = db.createObjectStore("videos-meta", { keyPath: "file" });

        // store binary data of the videos here
        var objectStoreBinary = db.createObjectStore("videos-binary");

        // Create an index to search customers by name. We may have duplicates
        // so we can't use a unique index.
        // objectStore.createIndex("name", "name", { unique: false });

        // Create an index to search customers by email. We want to ensure that
        // no two customers have the same email, so use a unique index.
        // objectStore.createIndex("email", "email", { unique: true });

        var generalOnComplete = function(event) {
            console.log('objectStore.transaction.oncomplete');
            // Store values in the newly created objectStore.
            // var customerObjectStore = db.transaction("customers", "readwrite").objectStore("customers");
            // for (var i in customerData) {
            //     customerObjectStore.add(customerData[i]);
            // }
        };

        // Use transaction oncomplete to make sure the objectStore creation is
        // finished before adding data into it.
        objectStoreMeta.transaction.oncomplete = generalOnComplete;
        objectStoreBinary.transaction.oncomplete = generalOnComplete;

    };

    // window.MediaSource = window.MediaSource || window.WebKitMediaSource;

    var fs = electron.fs,
        path = electron.path,
        app = electron.app,
        data_path = path.join(app.getPath('userData'), 'roger-data', 'videos'),
        directory_url = 'http://192.168.0.8:3000/directory',
        download_url = 'http://192.168.0.8:3000/checkout/', // support key for overall info and key/filename for chunks
        // directory_url = 'http://chris.strsocial.com:3000/directory',
        // download_url = 'http://chris.strsocial.com:3000/checkout/', // support key for overall info and key/filename for chunks
        fsExtra = require('fs-extra'),
        request = require('request');

    // reference for the view
    $scope.data_path = data_path;

    // electron.dialog.showErrorBox('Oppps!', 'Looks like we have an error!');

    // hide page till done loading
    // 0 == not loaded, 1 == loading, 2 == loaded
    $scope.is_loading = 0;

    // stores the list of videos
    $scope.directory = [];

    $scope.sources = [];

    // get list of video dirs (file system)
    // var videos_available = fs.readdirSync('chunks/videos');
    // var videos_path = combine('chunks/videos/offline.mp4');

    // check that a file is a dir with this example
    // var video_chunk_dir = fs.statSync('chunks/videos/.DS_Store');
    // console.log(video_chunk_dir.isDirectory());

    // loaad videos from the remote server directory listing
    var fetchRemoteVideoList = function() {
        $http.get(directory_url).then(function(res){
            // store video directory in local scope
            $scope.directory = [];

            // check status of each video 'downlaoded' status
            // push updated video objects to the $scope.directory
            angular.forEach(res.data.videos, function(video, key){
                video.is_downloaded = $scope.isDownloaded(video);
                this.push(video);
            }, $scope.directory);

            // update loading status when done
            $scope.is_loading = 2;
            // console.log('Loaded videos:', $scope.directory);
        });
    }

    var getVideoLocalPath = function(video) {
        var local_path = path.join(data_path, video.file);
        return local_path;
    }

    var dirExists = function(path) {
        try {
            // console.log('checking path', path);
            return fs.statSync(path).isDirectory();
        } catch (e) {
            return false;
        }
    }

    var fileExists = function(path) {
        // var dir = path.dirname(path);
        // if (!dirExists(dir)) {
        //     return false;
        // }

        try {
            // console.log('checking path', path);
            return fs.statSync(path).isFile();
        } catch (e) {
            return false;
        }
    }

    var partsInDir = function(path) {
        var stats = fs.readdirSync(path);

        // @TODO: We should filter filenames
        // e.g. mac: filter out .DS_Store, or even whitelist the ".chunk" files

        // console.log('dir stats for ' + path);
        // console.log(stats.length);

        return stats;
    }

    var triggerViewSync = function() {

        var updated_directory = [];

        // check status of each video 'downlaoded' status
        // push updated video objects to the $scope.directory
        angular.forEach($scope.directory, function(video, key){
            video.is_downloaded = $scope.isDownloaded(video);
            video.is_downloading = 0;
            video.download_progress = 100;
            video.is_in_database = false;
            this.push(video);
        }, updated_directory);

        console.log('updating $scope.directory');
        $scope.directory = updated_directory;

        // hacky fix to refresh the view
        setInterval(function() {
            $scope.$apply()
        }, 500);

        updated_directory = null;
    }

    var getVideoByKey = function(key) {
        console.log($scope.directory);
    }

    var writeVideoMetaToIndexDB = function(video) {
        console.log('writeVideoToIndexDB', video);
        var transaction = db.transaction(["videos-meta"], "readwrite");

        transaction.oncomplete = function(event) {
            console.log("Writing meta to indexdb done!");
        };

        var objectStore = transaction.objectStore("videos-meta");
        var request = objectStore.add(video);
        request.onsuccess = function(event) {
            console.log('Success')
            // event.target.result == customerData[i].ssn;
        };
    }

    var writeVideoBinaryToIndexDB = function(video, buf) {

        var storage_dir = getVideoLocalPath(video),
            parts = partsInDir(storage_dir),
            parts_count = parts.length;
            buffer_parts = [],
            parts_read = 0,
            current_part = 0;

        console.log('writeVideoBinaryToIndexDB', video);
        var transaction = db.transaction(["videos-binary"], "readwrite");

        transaction.oncomplete = function(event) {
            console.log("Writing binary to indexdb done!");
        };

        var readNextChunk = function() {
            current_part++;

            if (parts.length <= 0) {
                console.log('All chunks loaded');
                // media_source.endOfStream();
                return;
            } else {
                console.log('Reading part ' + current_part + ' / ' + parts_count);
            }

            // store parts of a single chunk
            var buffer_parts = [],
                buf = new Buffer(0);

            // take the next part in order
            console.log('Processing ' + part);
            var part = parts.shift(),
                file_path = path.join(storage_dir, part),
                stream = fs.createReadStream(file_path);

            stream.on('data', function(data) {
                console.log('Store the part of the buffer');
                buf = Buffer.concat([buf, data]);
            });

            stream.on('end', function() {

                // var new_blob = new Blob(buf, {type: 'video/mp4'});

                // complete_chunk_buffer = Buffer.concat(buffer_parts);
                console.log('write stram data to indexdb, append the current value');

                console.log('reading indexdb, looking for existing record');
                console.log('DEBUG: ', video.file);

                var transaction = db.transaction(["videos-binary"], "readwrite");
                var objectStore = transaction.objectStore("videos-binary");
                var db_request = objectStore.get(video.file);

                // get existing entry, it may be undefined
                db_request.onsuccess = function(event) {

                    var result = event.target.result;

                    console.log("FOUND data for " + video.file, event.target.result);

                    console.log(typeof result);

                    if (typeof result != 'object') {
                        result = new Buffer(0);
                    } else {
                        var unit_8_array = new Uint8Array(result);
                        result = new Buffer(result);
                    }

                    console.log(typeof result);

                    // write the new blob to the db

                    var updated_buffer = Buffer.concat([result, buf]);

                    // Put this updated object back into the database.
                    var requestUpdate = objectStore.put(updated_buffer, video.file);

                    requestUpdate.onerror = function(event) {
                        // Do something with the error
                        console.log('update error', event);
                    };
                    requestUpdate.onsuccess = function(event) {
                        // Success - the data is updated!
                        console.log('update success');
                        // appendVideoIndexDB(video, buf);

                        console.log('End chunk stream');
                        parts_read++;

                        readNextChunk();
                    };
                };

            });

            stream.on('error', function(err) {
                console.log('readStream: something is wrong :( ');
                stream.close();
            });
        }

        readNextChunk();





































        // var objectStore = transaction.objectStore("videos-binary");
        // var request = objectStore.add(video);
        // request.onsuccess = function(event) {
        //     console.log('Success')
        //     // event.target.result == customerData[i].ssn;
        // };


    }

    $scope.updateDownloadProgress = function(video) {
        console.log(video);
    }

    $scope.deleteVideo = function(video) {
        var delete_path = getVideoLocalPath(video);
        if (delete_path != data_path) {
            console.log('Path to delete', delete_path);

            // delete chunks from file system

            fsExtra.emptydirSync(delete_path);
            console.log('deleted chunks from disk');

            // delete from indexdb

            var request_meta = db.transaction(["videos-meta"], "readwrite")
                .objectStore("videos-meta")
                .delete(video.file);
            request_meta.onsuccess = function(event) {console.log('deleted indexdb meta entry');};

            var request_binary = db.transaction(["videos-binary"], "readwrite")
                .objectStore("videos-binary")
                .delete(video.file);
            request_binary.onsuccess = function(event) {console.log('deleted indexdb binary entry');};

            triggerViewSync();
        }
    }

    $scope.onClickFetchList = function() {
        console.log('Loading videos from remote server');
        fetchRemoteVideoList();
    }

    $scope.isDownloaded = function(video) {
        // console.log('Checking if video is downloaded', video);

        var is_downloaded = false;
        var local_path = getVideoLocalPath(video),
            video_parts;

        // console.log('Local Path: ', local_path);

        if (!dirExists(local_path)) {
            // console.log('video local path does not exist, so it\'s not downloaded.');
            is_downloaded = false;
        } else {
            var video_parts = partsInDir(local_path);
            if (video_parts.length == parseInt(video.parts_count)) {
                is_downloaded = true;
            } else {
                is_downloaded = false;
            }
        }

        // console.log(video, is_downloaded);

        return is_downloaded;
    }

    $scope.downloadVideo = function(video) {
        console.log('downloadVideo', video);

        video.is_downloading = true;
        video.download_progress = 0;

        var url = download_url + video.key,
            parts = parseInt(video.parts_count), // each part is an object with filename and url
            storage_dir = getVideoLocalPath(video),
            chunks_to_download = [],
            chunks_total = 0,
            chunks_downloaded = 0;

        var updateVideoProgress = function() {
            try {
                console.log({
                    chunks_downloaded: chunks_downloaded,
                    chunks_total: chunks_total,
                    download_progress: (chunks_downloaded / chunks_total) * 100
                });

                video.download_progress = (chunks_downloaded / chunks_total) * 100;
            } catch (e) {
                console.log('error thrown', chunks_downloaded, chunks_total);
                // catch division by zero, edge case but maybe possible
                video.download_progress = 0;
            }

            if (video.download_progress >= 100) {
                console.log('trigger view sync');
                video.is_downloading = false;
                video.download_progress = 100;
                triggerViewSync();
            }

            console.log('progress: ', video.download_progress);
        }

        var downloadNextChunk = function() {

            if (chunks_to_download.length == 0) {
                console.log('All chunks downloaded');
                updateVideoProgress();
                writeVideoMetaToIndexDB(video);
                writeVideoBinaryToIndexDB(video);
                return;
            } else {
                console.log('chunks remaining', chunks_to_download.length);
                updateVideoProgress();
            }

            // get the next chunk
            var chunk = chunks_to_download.pop();

            // var fs = require('fs');
            var stream = request(chunk.url),
                file_path = path.join(storage_dir, chunk.filename),
                writeStream = fs.createWriteStream(file_path);

            stream.on('data', function(data) {
                // console.log('write stram data');
                writeStream.write(data);
            });

            stream.on('end', function() {
                console.log('Saved chunk to ' + chunk.filename);
                chunks_downloaded++;
                downloadNextChunk();
                writeStream.end();
            });

            stream.on('error', function(err) {
                console.log('something is wrong :( ');
                writeStream.close();
            });
        }

        // get video details
        var startVideoDownload = function(url, video) {
            $http.get(url).then(function(res){
                // check status of each video 'downlaoded' status
                // push updated video objects to the $scope.directory
                angular.forEach(res.data.parts, function(chunk, key){
                    console.log('Checking if chunk exists: ', path.join(storage_dir, chunk.filename));
                    chunks_total++;
                    if (!fileExists(path.join(storage_dir, chunk.filename))) {
                        // download the chunk, and write the result to a file
                        chunks_to_download.push(chunk);
                    } else {
                        chunks_downloaded++;
                    }

                    updateVideoProgress(video);
                });

                console.log('Chunks that need to be downloaded', chunks_to_download);

                downloadNextChunk(video);

                // // @see http://stackoverflow.com/questions/31581254/how-to-write-a-file-from-an-arraybuffer-in-js
                // $http.get(chunk.url, {responseType: 'arraybuffer'}).then(function(res){
                //     var file_path = path.join(storage_dir, chunk.filename);
                //     var unit_8_array = new Uint8Array(res.data);
                //     fs.writeFileSync(file_path, new Buffer(unit_8_array));
                //
                //     triggerViewSync();
                // });
            });
        }

        startVideoDownload(url, video);

        // make sure we have a directory to check
        if (!dirExists(storage_dir)) {
            fsExtra.mkdirsSync(storage_dir);
        }

        return;
    }

    $scope.playVideo = function(video) {
        console.log('playVideo');

        var transaction = db.transaction(["videos-binary"], "readwrite");
        var objectStore = transaction.objectStore("videos-binary");
        var db_request = objectStore.get(video.file);

        // get existing entry, it may be undefined
        db_request.onsuccess = function(event) {
            console.log('loaded file');
            console.log(event.target.result);

            var binary = event.target.result;
            var blob = new Blob([binary], { type:'video/mp4' } );

            var URL = window.URL || window.webkitURL;
            var videoURL = URL.createObjectURL(blob);
            var video = document.querySelector('video');
            video.src = videoURL;
        }




        return;















        var url = download_url + video.key,
            storage_dir = getVideoLocalPath(video),
            parts = partsInDir(storage_dir),
            parts_count = parts.length;
            buffer_parts = [],
            parts_read = 0,
            current_part = 0;

        // angular.forEach(parts, function(part, key){
        //     var file_path = path.join(storage_dir, part),
        //
        //     console.log("Read file system");
        //     var buffer_part = fs.readFileSync(file_path);
        //     buffer_parts.push(buffer_part);
        //     buffer_part = null;
        //     // console.log('size', info.size);
        //
        //     // var buffer_part = new Buffer(info.size);
        //     // var buffer_part = fs.readFileSync(file_path);
        //     // console.log('length', buffer_part.length);
        //
        //     // var total_length = buffer_full.length + buffer_part.length;
        //     // buffer_full = Buffer.concat([buffer_full, buffer_part]);
        // });

        //
        // using MediaSource
        //
        console.log('Creating a new MediaSource()');
        var media_source = new MediaSource(),
            source_buffer,
            video_el = document.querySelector('video'),
            large_blob = new Blob();

        video_el.src = window.URL.createObjectURL(media_source);

        var readNextChunk = function() {
            current_part++;

            if (parts.length <= 0) {
                console.log('All chunks loaded');
                // media_source.endOfStream();
                return;
            } else {
                console.log('Reading part ' + current_part + ' / ' + parts_count);
            }

            // store parts of a single chunk
            var buffer_parts = [],
                buf = new Buffer(0);

            // take the next part in order
            console.log('Processing ' + part);
            var part = parts.shift(),
                file_path = path.join(storage_dir, part),
                stream = fs.createReadStream(file_path);

            stream.on('data', function(data) {
                console.log('Store the part of the buffer');
                buf = Buffer.concat([buf, data]);
            });

            stream.on('end', function() {
                // complete_chunk_buffer = Buffer.concat(buffer_parts);
                console.log('write stram data to indexdb, append the current value');
                // get existing entry
                appendVideoIndexDB(video, buf);

                console.log('End chunk stream');
                parts_read++;
                // stream.end();
                readNextChunk();
            });

            stream.on('error', function(err) {
                console.log('readStream: something is wrong :( ');
                stream.close();
            });
        }

        media_source.addEventListener('sourceopen', function(e) {
            console.log('media_source.addSourceBuffer')
            source_buffer = media_source.addSourceBuffer('video/webm; codecs="vp8, vorbis"');

            console.log('Starting to read chunks');
            readNextChunk();
        }, false);

        // source_buffer = media_source.addSourceBuffer('video/mp4');
        // video_el.src = window.URL.createObjectURL(media_source);

        // console.log('Starting to read chunks');
        // readNextChunk();

        // //
        // // using file readFile
        // //
        // var processNextPart = function() {
        //     current_part++;
        //     console.log('Reading part ' + current_part + ' / ' + parts_count);
        //
        //     // take the next part in order
        //     console.log('Processing ' + part);
        //     var part = parts.shift(),
        //         file_path = path.join(storage_dir, part);
        //
        //     console.log("Read file system");
        //     fs.readFile(file_path, function(err, buffer_part){
        //         console.log('Done reading file system')
        //         buffer_parts.push(buffer_part);
        //         buffer_part = null;
        //
        //         if (parts.length > 0) {
        //             // process the next part if we have more parts
        //             processNextPart();
        //         } else {
        //             // if we're done reading all the parts, we can load parts into the player
        //             loadPartsIntoPlayer();
        //         }
        //     });
        // }
        //
        // var loadPartsIntoPlayer = function() {
        //     console.log('combining ' + buffer_parts.length + ' buffers into one buffer');
        //     var buffer_full = Buffer.concat(buffer_parts);
        //     buffer_parts = null;
        //
        //     // test write file_path
        //     // fs.writeFileSync(path.join(app.getPath('desktop'), 'test.mp4'), buffer_full);
        //
        //     console.log('Creating new Blob');
        //     // var data = buffer_full.toString('utf8');
        //     var data = new Blob( [ buffer_full ], {type: "video/mp4"} );
        //
        //     console.log('Creating video.src with createObjectURL');
        //     var URL = window.URL || window.webkitURL;
        //     var video_src = URL.createObjectURL(data);
        //
        //     console.log('Setting video.src');
        //     var video_el = document.querySelector('video');
        //     video_el.src = video_src;
        //     video_el.play();
        // }
        //
        // // start Processing
        // processNextPart();
        //
        // // $scope.sources = [{path: video_src, type: 'video/mp4'}];

        return;
    }
}]);
