@echo OFF
echo 'start int'
echo %CD%
reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==32BIT start "" "Quick Start\Quick Start-win32-ia32\Quick Start.exe"
if %OS%==64BIT start "" "Quick Start\Quick Start-win32-x64\Quick Start.exe"

