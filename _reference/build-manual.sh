#!/bin/bash

# ensure we have nodeenv installed and available
DIST_PATH="${DIST_PATH:-./dist}"
PACKAGER_DIR='OfflineLectures'
NODEENV_PATH="${NODEENV_PATH:-env}"
DATE=`date +%Y%m%d%H%M%S`
BUILD_VERSION="${BUILD_VERSION:-$DATE}"
APP_VERSION="${APP_VERSION:-2.0.0}"
DO_SIGN_MAC_BUILD="${DO_SIGN_MAC_BUILD:-no}"
CODE_SIGNING_IDENTITY="${CODE_SIGNING_IDENTITY:-3rd Party Mac Developer Application: Roger Philipp CPA Review (9ATMUE9N6P)}"
OSX_BUNDLE_ID='com.rogercpareview.olv'

MAC_ONLY=${MAC_ONLY:-no}
WIN_ONLY=${WIN_ONLY:-no}

echo "App Version: $APP_VERSION"
echo "Build Version: $BUILD_VERSION"
echo "Dist path: $DIST_PATH"
echo "Code Sign Mac?: $DO_SIGN_MAC_BUILD"

# install nodeenv if not yet enabled
if [ ! -d $NODEENV_PATH ]
then
    nodeenv --node=5.10.1 --prebuilt $NODEENV_PATH
fi

# enable our nodeenv
source $NODEENV_PATH/bin/activate

# install npm packages
npm install

# let's add our npm bin to our path
export PATH=$PATH:./node_modules/.bin

# install top-level development deps (used only for packaging / dev)
npm install

# install app deps (included in dist)
npm install --production --prefix ./app-main ./app-main

# copy various files into the dist dir
# cp app-usb-dist/settings.json $DIST_PATH/settings.json

# package the app into our dist folder

# All platforms
#
# app-copyright      human-readable copyright line for the app
# app-version        release version to set for the app
# asar               packages the source code within your app into an archive
# asar-unpack        unpacks the files to app.asar.unpacked directory whose filenames regex .match
#                    this string
# asar-unpack-dir    unpacks the dir to app.asar.unpacked directory whose names glob pattern or
#                    exactly match this string. It's relative to the <sourcedir>.
# build-version      build version to set for the app
# cache              directory of cached Electron downloads. Defaults to `$HOME/.electron`
#                    (Deprecated, use --download.cache instead)
# download           a list of sub-options to pass to electron-download. They are specified via dot
#                    notation, e.g., --download.cache=/tmp/cache
#                    Properties supported:
#                    - cache: directory of cached Electron downloads. Defaults to `$HOME/.electron`
#                    - mirror: alternate URL to download Electron zips
#                    - strictSSL: whether SSL certs are required to be valid when downloading
#                      Electron. Defaults to true, use --download.strictSSL=false to disable checks.
# icon               the icon file to use as the icon for the app. Note: Format depends on platform.
# ignore             do not copy files into app whose filenames regex .match this string
# out                the dir to put the app into at the end. defaults to current working dir
# overwrite          if output directory for a platform already exists, replaces it rather than
#                    skipping it
# prune              runs `npm prune --production` on the app
# strict-ssl         whether SSL certificates are required to be valid when downloading Electron.
#                    It defaults to true, use --strict-ssl=false to disable checks.
#                    (Deprecated, use --download.strictSSL instead)
# tmpdir             temp directory. Defaults to system temp directory, use --tmpdir=false to disable
#                    use of a temporary directory.
# version            the version of Electron that is being packaged, see
#                    https://github.com/electron/electron/releasesspawn wine ENOENT

# GENERIC_FLAGS="--out ./dist --ignore 'node_modules/.bin' --asar --build-version=$BUILD_VERSION --app-version=$APP_VERSION --overwrite --prune"
# GENERIC_FLAGS="--out ./dist --build-version=$BUILD_VERSION --app-version=$APP_VERSION --overwrite --prune"

GENERIC_FLAGS="./app-main "OfflineLectures" \
    --version=0.37.8 \
    --out ./dist \
    --asar=true \
    --build-version=$BUILD_VERSION \
    --app-version=$APP_VERSION \
    --overwrite \
    --prune"

# electron-packager ./app-main \"Project\" --out=dist/win --platform=win32 --arch=ia32 --version=0.29.1 --icon=build/resources/icon.ico --version-string.CompanyName=\"My Company\" --version-string.ProductName=\"Project\" --version-string.FileDescription=\"Project\" 

# darwin/mas target platforms only
#
# app-bundle-id      bundle identifier to use in the app plist
# app-category-type  the application category type
#                    For example, `app-category-type=public.app-category.developer-tools` will set the
#                    application category to 'Developer Tools'.
# extend-info        a plist file to append to the app plist
# extra-resource     a file to copy into the app's Contents/Resources
# helper-bundle-id   bundle identifier to use in the app helper plist
# osx-sign           (OSX host platform only) Whether to sign the OSX app packages. You can either
#                    pass --osx-sign by itself to use the default configuration, or use dot notation
#                    to configure a list of sub-properties, e.g. --osx-sign.identity="My Name"
#                    Properties supported:
#                    - identity: should contain the identity to be used when running `codesign`
#                    - entitlements: the path to entitlements used in signing
#                    - entitlements-inherit: the path to the 'child' entitlements


if [ "$WIN_ONLY" == 'no' ]
then
    echo ': Starting Mac build'

    ./node_modules/.bin/electron-packager $GENERIC_FLAGS \
        --icon=app-main/img/icons/icon.icns \
        --arch=x64 --platform=darwin

    if [ "$DO_SIGN_MAC_BUILD" == "yes" ]
    then
        echo ': Code signing Mac build'

        ./node_modules/.bin/electron-osx-sign $DIST_PATH/$PACKAGER_DIR-darwin-x64/OfflineLectures.app \
            --app-bundle-id=$OSX_BUNDLE_ID \
            --identity="$CODE_SIGNING_IDENTITY"
    fi

    # for some reason creates without permissions to move dirs
    chown -R $(whoami) $DIST_PATH/$PACKAGER_DIR-darwin-x64
    # clear the way for our app
    rm -rf $DIST_PATH/mac-osx
    # move our dirs into a more human-friendly directory, electron-packager
    mv $DIST_PATH/OfflineLectures-darwin-x64 $DIST_PATH/mac-osx
fi

# win32 target platform only
#
# version-string     a list of sub-properties used to set the application metadata embedded into
#                    the executable. They are specified via dot notation,
#                    e.g. --version-string.CompanyName="Company Inc."
#                    or --version-string.ProductName="Product"
#                    Properties supported:
#                    - CompanyName
#                    - FileDescription
#                    - OriginalFilename
#                    - ProductName
#                    - InternalName

if [ "$MAC_ONLY" == 'no' ]
then
    if [ $(which wine) ]
    then
        echo ': Starting Windows build'

        ./node_modules/.bin/electron-packager $GENERIC_FLAGS \
            --icon=app-main/img/icons/icon.ico \
            --arch=ia32 --platform=win32 \
            --version-string.CompanyName="Roger CPA Review" \
            --version-string.ProductName="Offline Lectures" \
            --version-string.FileDescription="OfflineLectures"

        chown -R $(whoami) $DIST_PATH/$PACKAGER_DIR-win32-ia32
        rm -rf $DIST_PATH/windows
        mv $DIST_PATH/OfflineLectures-win32-ia32 $DIST_PATH/windows

        ./node_modules/.bin/electron-packager $GENERIC_FLAGS \
            --icon=app-main/img/icons/icon.ico \
            --arch=x64 --platform=win32 \
            --version-string.CompanyName="Roger CPA Review" \
            --version-string.ProductName="Offline Lectures" \
            --version-string.FileDescription="OfflineLectures"

        chown -R $(whoami) $DIST_PATH/$PACKAGER_DIR-win32-x64
        rm -rf $DIST_PATH/windows64
        mv $DIST_PATH/$PACKAGER_DIR-win32-x64 $DIST_PATH/windows64


    else
        echo 'Skipping win32 platform because I could not find wine installed.'
        echo 'Try: $ brew install wine'
        echo 'Check: $ which wine'
    fi
fi

# disable our node, may not be required
deactivate_node
