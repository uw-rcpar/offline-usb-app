#!/bin/bash

USBPATH=${USBPATH:-/Volumes/ROGERCPA}
VIDEO_WORKSPACE=${$VIDEO_WORKSPACE:-/Volumes/FantomHD/Data/roger/offline-lectures/workspace}
WORKSPACE=${WORKSPACE:-.}

mkdir -p $USBPATH/.videos

# copy videos dir if not on the dist path
if [ ! -d $USBPATH/.videos ]
then
    cp -rf $WORKSPACE/dist/usb/.videos $USBPATH/.videos
fi

rsync -a $WORKSPACE/dist/usb/* $USBPATH/

rsync -a \
    $VIDEO_WORKSPACE/encode-videos/encoding-vm/build/output/webm_crypt/* \
    $USBPATH/.videos/
