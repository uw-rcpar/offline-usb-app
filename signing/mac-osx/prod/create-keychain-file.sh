#!/bin/bash

# @see: http://www.kylethielk.com/blog/build-ios-app-with-jenkins-code-signing-issues/

#Create keychain
security create-keychain -p CPAdev2013 RogerCpaOlv.keychain

#Import Cert/Key - The -A flag is what allows non-interactive use of cert
security import mac_app.cer -t cert -k RogerCpaOlv.keychain -A
security import Certificates.p12 -t agg -k RogerCpaOlv.keychain -A

echo ""
echo "Your keychain should exist now under ~/Library/Keychains/RogerCpaOlv.keychain..."
echo "I'll try to move it here."
echo ""

mv ~/Library/Keychains/RogerCpaOlv.keychain .
