KeyChain File Info
==================

This keychain contains the private key and code signing identity for both
the 'mas-cert' (for mac app store dist) as well as for the 'developer-id-cert'
which is used for distribution outside the mac app store.

The password for the keychain file is 'CPAdev2013', and can be viewed or edited
using any recent Mac / OSX computer with the KeyChain Access app.

We use the keychain file in builds when a prodution dist needs to be signed.