export PATH=$PATH:/usr/local/bin/signcode

# List of versions to build
RELEASE_2016_FULL=no
RELEASE_2016_CRAM=no
RELEASE_2017_FULL=no
RELEASE_2017_CRAM=yes

# export MAC_ONLY=yes

# Tell our build script to also sign the app (when running manually you'll be promted for the certificate
# password when the build starts to sign the mac builds.)
export DO_SIGN_MAC_BUILD=yes

# This will override auth urls in each build.
#export SERVICE_AUTH_URL='https://www.rogercpareview.com/user/ssologin/offline-lec'
export SERVICE_AUTH_URL='https://www.rogercpareview.com/user/ssologin/offline-lec'
export WORKSPACE=$(pwd)

echo ""
echo "WORKSPACE: $WORKSPACE"
echo ""

echo "====================================="


if [ "$RELEASE_2017_FULL" == 'yes' ]
then
    export APP_MODE=full
    export BUNDLE_VERSION=2017

    echo ""
    echo "Creating zip $APP_MODE"
    echo "Creating zip $BUNDLE_VERSION"
    echo ""

    rm -rf ./dist/usb/*
    bin/build-dist.sh
    bin/win-signcode.sh

    echo ""
    echo "Creating zip archive"
    echo ""

    # change to the dist directory
    echo""
    echo "cp -Rfp dist/usb ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION"
    echo""
    rm -rf ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION
    cp -Rfp dist/usb ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION
    # ditto -c -k --sequesterRsrc --keepParent . $WORKSPACE/dist-$APP_MODE-$BUNDLE_VERSION.zip
    # cd $WORKSPACE

    echo ""
    echo "Done creating zip archive"
    echo ""

    echo "====================================="
fi


if [ "$RELEASE_2016_FULL" == 'yes' ]
then
    export APP_MODE=full
    export BUNDLE_VERSION=2016

    echo ""
    echo "Creating zip $APP_MODE"
    echo "Creating zip $BUNDLE_VERSION"
    echo ""

    rm -rf ./dist/usb/*
    bin/build-dist.sh
    bin/win-signcode.sh

    echo ""
    echo "Creating zip archive"
    echo ""

    # change to the dist directory
    echo""
    echo "cp -Rfp dist/usb ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION"
    echo""
    rm -rf ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION
    cp -Rfp dist/usb ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION
    # ditto -c -k --sequesterRsrc --keepParent . $WORKSPACE/dist-$APP_MODE-$BUNDLE_VERSION.zip
    # cd $WORKSPACE

    echo ""
    echo "Done creating zip archive"
    echo ""

    echo "====================================="
fi


if [ "$RELEASE_2017_CRAM" == 'yes' ]
then
    export APP_MODE=cram
    export BUNDLE_VERSION=2017

    echo ""
    echo "Creating zip $APP_MODE"
    echo "Creating zip $BUNDLE_VERSION"
    echo ""

    rm -rf ./dist/usb/*
    bin/build-dist.sh
    bin/win-signcode.sh

    echo ""
    echo "Creating zip archive"
    echo ""

    # change to the dist directory
    echo""
    echo "cp -Rfp dist/usb ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION"
    echo""
    rm -rf ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION
    cp -Rfp dist/usb ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION
    # ditto -c -k --sequesterRsrc --keepParent . $WORKSPACE/dist-$APP_MODE-$BUNDLE_VERSION.zip
    # cd $WORKSPACE

    echo ""
    echo "Done creating zip archive"
    echo ""

    echo "====================================="

fi


if [ "$RELEASE_2016_CRAM" == 'yes' ]
then

    export APP_MODE=cram
    export BUNDLE_VERSION=2016

    echo ""
    echo "Creating zip $APP_MODE"
    echo "Creating zip $BUNDLE_VERSION"
    echo ""

    rm -rf ./dist/usb/*
    bin/build-dist.sh
    bin/win-signcode.sh

    echo ""
    echo "Creating zip archive"
    echo ""

    # change to the dist directory
    echo""
    echo "cp -Rfp dist/usb ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION"
    echo""
    rm -rf ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION
    cp -Rfp dist/usb ~/Desktop/rogercpa-dist/$APP_MODE-$BUNDLE_VERSION
    # ditto -c -k --sequesterRsrc --keepParent . $WORKSPACE/dist-$APP_MODE-$BUNDLE_VERSION.zip
    # cd $WORKSPACE

    echo ""
    echo "Done creating zip archive"
    echo ""
fi