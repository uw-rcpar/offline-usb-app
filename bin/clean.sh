#!/bin/bash

# dist folders
rm -rf ./dist/src
rm -rf ./dist/tmp
rm -rf ./dist/usb/windows
rm -rf ./dist/usb/mac-osx

# we keep this file / directory, it's marked as hidden in windows
# we can use it and copy to dist
touch ./dist/usb/.videos/.gitkeep

# remove node packages
rm -rf ./env
rm -rf ./node_modules
rm -rf ./app-main/node_modules

echo 'Manually ensure you run deactivate_node'
