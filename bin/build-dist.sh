#!/bin/bash

# ensure we have nodeenv installed and available
USE_VIRTUAL_ENV="${USE_VIRTUAL_ENV:-true}"
DIST_PATH="${DIST_PATH:-dist/usb}"
TMP_PATH="${TMP_PATH:-dist/tmp}"

# We can pass this env var at run-time (e.g. via CI server like Jenkins)
# Do not set it by default here, because the JSON file will set this if it's not defined in the ENV var.
# SERVICE_AUTH_URL="${SERVICE_AUTH_URL:-https://www.rogercpareview.com/user/ssologin/offline-lec}"

DO_INSTALL_NPM_PACKS=${DO_INSTALL_NPM_PACKS:-true}
ELECTRON_PREBUILT_DIR=dist/src
PACKAGER_DIR='OfflineLectures'
APP_NAME=$PACKAGER_DIR
NODEENV_PATH="${NODEENV_PATH:-env}"
DATE=`date +%Y%m%d%H%M%S`
BUILD_VERSION="${BUILD_VERSION:-$DATE}"
APP_VERSION="${APP_VERSION:-2.1.0}"
# ELECTRON_VERSION=0.37.8
ELECTRON_VERSION=1.4.13
DO_SIGN_MAC_BUILD="${DO_SIGN_MAC_BUILD:-no}"
# CODE_SIGNING_IDENTITY="${CODE_SIGNING_IDENTITY:-3rd Party Mac Developer Application: Roger Philipp CPA Review (9ATMUE9N6P)}"
CODE_SIGNING_IDENTITY="${CODE_SIGNING_IDENTITY:-Developer ID Application: Roger Philipp CPA Review (9ATMUE9N6P)}"
OSX_BUNDLE_ID='9ATMUE9N6P.com.rogercpareview.olv'

MAC_ONLY=${MAC_ONLY:-no}
WIN_ONLY=${WIN_ONLY:-no}

echo "App Version: $APP_VERSION"
echo "Build Version: $BUILD_VERSION"
echo "Dist path: $DIST_PATH"
echo "Code Sign Mac?: $DO_SIGN_MAC_BUILD"
echo "Use Virtual Env : $USE_VIRTUAL_ENV "
echo ""
echo "APP_MODE: $APP_MODE"
echo "BUNDLE_VERSION: $BUNDLE_VERSION"
echo ""

#
# install nodeenv if not yet enabled
#

# USE_VIRTUAL_ENV=$USE_VIRTUAL_ENV NODEENV_PATH=$NODEENV_PATH bin/setup-npm.sh

if [ "$USE_VIRTUAL_ENV" = true ]
then
    echo "Using Virtual Enviroment (nodeenv for node.js)"
    if [ ! -d $NODEENV_PATH ]
    then
        nodeenv --node=5.10.1 --prebuilt $NODEENV_PATH
    fi
    # enable our nodeenv
    source $NODEENV_PATH/bin/activate
fi

#
# install npm packages we'll need
# these are top-level npm packages for development
# deps (used only for packaging / dev, not within the app)
#

# if [ "$DO_INSTALL_NPM_PACKS" = true ]
# then
# - ensure we have deps as expected globally
# - install electron-osx-sign for more customization
echo "Installing global NPM packages"
npm install -g electron-packager@"^8.4.0"
npm install -g electron-osx-sign@"^0.4.4"
npm install -g json@"^9.0.4"
echo "Installing local NPM packages (per package.json)"
npm install
# fi

# let's add our npm bin to our path
export PATH=$PATH:./node_modules/.bin

#
# copy the settings json file to be used by the app
#

if [[ -n "$BUNDLE_VERSION" &&  -n "$APP_MODE" ]]
then
    echo ""
    echo "Copying bundle version=$BUNDLE_VERSION and app_mode = $APP_MODE settings file";
    echo "cp app-main/settings-$APP_MODE-$BUNDLE_VERSION.json app-main/settings.json"
    cp app-main/settings-$APP_MODE-$BUNDLE_VERSION.json app-main/settings.json || { echo "Settings file not found, exiting with error."; echo ""; exit 1; }
else
    if [ "$APP_MODE" == "cram" ]
    then
        echo ""
        echo "cp app-main/settings-cram.json app-main/settings.json"
        cp app-main/settings-cram.json app-main/settings.json || { echo "Settings file not found, exiting with error."; echo ""; exit 1; }
    else
         #defaults
        echo ""
        echo "cp app-main/settings-full.json app-main/settings.json"
        cp app-main/settings-full.json app-main/settings.json || { echo "Settings file not found, exiting with error."; echo ""; exit 1; }
    fi
fi

echo ""

#
# update endpoint url if an ENV var is provided, otherwise we use
# the one provided in the source json file
#

if [ -n "$SERVICE_AUTH_URL" ]
then
    # on OSX we'll need to use gsed
    if [ ! $(which json) ]
    then
        echo 'CLI tool `json` not found, use npm to install it before continuing.'
        exit 1;
    else
        echo 'CLI tool `json` found, editing app-main/settings.json now.'
    fi

    # The first step is escaping slashes from the url string to avoid conflicts with sed regexp
    # SERVICE_AUTH_URL=$(echo "$SERVICE_AUTH_URL" | sed 's/\//\\\//g')
    JSON_CMD="json -4 -I -f app-main/settings.json -e 'this.service_auth_url=\"$SERVICE_AUTH_URL\"'"
    echo  "Updating url to $SERVICE_AUTH_URL"

    # now update the url in the json field
    echo $JSON_CMD
    eval $JSON_CMD
else
    echo "SERVICE_AUTH_URL env var not defind - skipping"
fi

echo ""

# install app deps (included in dist)
# cd app-main
# npm install --production
# cd ..
# instead, try deleting the modules from that directory
# rm -rf ./app-main/node_modules

# copy various files into the dist dir
# cp app-usb-dist/settings.json $DIST_PATH/settings.json

# package the app into our dist folder

# All platforms
#
# app-copyright      human-readable copyright line for the app
# app-version        release version to set for the app
# asar               packages the source code within your app into an archive
# asar-unpack        unpacks the files to app.asar.unpacked directory whose filenames regex .match
#                    this string
# asar-unpack-dir    unpacks the dir to app.asar.unpacked directory whose names glob pattern or
#                    exactly match this string. It's relative to the <sourcedir>.
# build-version      build version to set for the app
# cache              directory of cached Electron downloads. Defaults to `$HOME/.electron`
#                    (Deprecated, use --download.cache instead)
# download           a list of sub-options to pass to electron-download. They are specified via dot
#                    notation, e.g., --download.cache=/tmp/cache
#                    Properties supported:
#                    - cache: directory of cached Electron downloads. Defaults to `$HOME/.electron`
#                    - mirror: alternate URL to download Electron zips
#                    - strictSSL: whether SSL certs are required to be valid when downloading
#                      Electron. Defaults to true, use --download.strictSSL=false to disable checks.
# icon               the icon file to use as the icon for the app. Note: Format depends on platform.
# ignore             do not copy files into app whose filenames regex .match this string
# out                the dir to put the app into at the end. defaults to current working dir
# overwrite          if output directory for a platform already exists, replaces it rather than
#                    skipping it
# prune              runs `npm prune --production` on the app
# strict-ssl         whether SSL certificates are required to be valid when downloading Electron.
#                    It defaults to true, use --strict-ssl=false to disable checks.
#                    (Deprecated, use --download.strictSSL instead)
# tmpdir             temp directory. Defaults to system temp directory, use --tmpdir=false to disable
#                    use of a temporary directory.
# version            the version of Electron that is being packaged, see
#                    https://github.com/electron/electron/releasesspawn wine ENOENT

# GENERIC_FLAGS="--out ./dist --ignore 'node_modules/.bin' --asar --build-version=$BUILD_VERSION --app-version=$APP_VERSION --overwrite --prune"
# GENERIC_FLAGS="--out ./dist --build-version=$BUILD_VERSION --app-version=$APP_VERSION --overwrite --prune"

GENERIC_FLAGS="./app-main "$APP_NAME" \
    --electron-version="$ELECTRON_VERSION" \
    --build-version=$BUILD_VERSION \
    --app-version=$APP_VERSION \
    --asar=true \
    --download.cache=$ELECTRON_PREBUILT_DIR \
    --tmpdir=$TMP_PATH \
    --out=$DIST_PATH \
    --overwrite"
    # --prune"

# electron-packager ./app-main \"Project\" --out=dist/win --platform=win32 --arch=ia32 --version=0.29.1 --icon=build/resources/icon.ico --version-string.CompanyName=\"My Company\" --version-string.ProductName=\"Project\" --version-string.FileDescription=\"Project\"

# darwin/mas target platforms only
#
# app-bundle-id      bundle identifier to use in the app plist
# app-category-type  the application category type
#                    For example, `app-category-type=public.app-category.developer-tools` will set the
#                    application category to 'Developer Tools'.
# extend-info        a plist file to append to the app plist
# extra-resource     a file to copy into the app's Contents/Resources
# helper-bundle-id   bundle identifier to use in the app helper plist
# osx-sign           (OSX host platform only) Whether to sign the OSX app packages. You can either
#                    pass --osx-sign by itself to use the default configuration, or use dot notation
#                    to configure a list of sub-properties, e.g. --osx-sign.identity="My Name"
#                    Properties supported:
#                    - identity: should contain the identity to be used when running `codesign`
#                    - entitlements: the path to entitlements used in signing
#                    - entitlements-inherit: the path to the 'child' entitlements


if [ "$WIN_ONLY" == 'no' ]
then
    echo ''
    echo 'Starting Mac build'
    # echo ":= prep npm modules"
    cd app-main
    npm prune --production
    npm install --production
    # ../node_modules/.bin/electron-rebuild --version=$ELECTRON_VERSION \
    #     --electron-prebuilt-dir=../$ELECTRON_PREBUILT_DIR \
    #     --arch=x64
    cd ..

    echo ": darwin x64 : build dist"
    # ./node_modules/.bin/electron-packager $GENERIC_FLAGS \
    electron-packager $GENERIC_FLAGS \
        --arch=x64 \
        --platform=darwin \
        --prune \
        --icon=app-main/img/icons/icon.icns

    # for some reason creates without permissions to move dirs
    echo ": darwin x64 : fix ownership so we can move the directory"
    chown -R $(whoami) $DIST_PATH/$PACKAGER_DIR-darwin-x64

    echo ": darwin x64 : rename dist dirs (move to mac-osx)"
    # clear the way for our app
    rm -rf $DIST_PATH/mac-osx
    # move our dirs into a more human-friendly directory, electron-packager
    mv $DIST_PATH/$PACKAGER_DIR-darwin-x64 $DIST_PATH/mac-osx

    if [ "$DO_SIGN_MAC_BUILD" == "yes" ]
    then
        echo ': darwin x64 : code signing Mac build'
        echo ""

        echo ": darwin x46 : using electron-osx-sign from the following path"
        which electron-osx-sign
        echo ""

        # ./node_modules/.bin/electron-osx-sign $DIST_PATH/$PACKAGER_DIR-darwin-x64/$APP_NAME.app \
        DEBUG='electron-osx-sign*'
        SIGN_CMD="DEBUG='electron-osx-sign*'"
        # SIGN_CMD="$SIGN_CMD electron-osx-sign $DIST_PATH/$PACKAGER_DIR-darwin-x64/$APP_NAME.app"
        SIGN_CMD="$SIGN_CMD electron-osx-sign $DIST_PATH/mac-osx/$APP_NAME.app"
        SIGN_CMD="$SIGN_CMD --app-bundle-id=$OSX_BUNDLE_ID"
        SIGN_CMD="$SIGN_CMD --identity=\"$CODE_SIGNING_IDENTITY\""
        SIGN_CMD="$SIGN_CMD --platform=darwin"
        SIGN_CMD="$SIGN_CMD --version=\"$ELECTRON_VERSION\""
        SIGN_CMD="$SIGN_CMD --provisioning-profile=signing/mac-osx/prod/developer-id-cert/Offline_Lectures_Dist_DEVID.provisionprofile"
        # SIGN_CMD="$SIGN_CMD --entitlements=signing/mac-osx/prod/entitlements.darwin.plist"
        # SIGN_CMD="$SIGN_CMD --entitlements-inherit=signing/mac-osx/prod/entitlements.darwin.inherit.plist"

        # SIGN_CMD2="codesign --force --verify --verbose --sign"
        # SIGN_CMD2="$SIGN_CMD2 \"$CODE_SIGNING_IDENTITY\""
        # SIGN_CMD2="$SIGN_CMD2 \"$DIST_PATH/mac-osx/$APP_NAME.app\""

        # eval $SIGN_CMD2

        echo ": darwin x46 : executing the following command to sign the app"
        echo "$SIGN_CMD"
        echo ""

        eval $SIGN_CMD

    fi

    echo ": darwin x64 : dist finished"
    echo ": path: $DIST_PATH/mac-osx"
    echo "============================="
fi

# win32 target platform only
#
# version-string     a list of sub-properties used to set the application metadata embedded into
#                    the executable. They are specified via dot notation,
#                    e.g. --version-string.CompanyName="Company Inc."
#                    or --version-string.ProductName="Product"
#                    Properties supported:
#                    - CompanyName
#                    - FileDescription
#                    - OriginalFilename
#                    - ProductName
#                    - InternalName

if [ "$MAC_ONLY" == 'no' ]
then
    echo ''
    echo 'Starting Windows build'

    cd app-main
    npm prune --production
    npm install --production
    cd ..


    echo ": win32 ia32 : build dist"
    electron-packager $GENERIC_FLAGS \
        --arch=ia32 \
        --platform=win32 \
        --prune \
        --icon=app-main/img/icons/icon.ico \
        --version-string.CompanyName="RogerCPA Review" \
        --version-string.FileDescription="$APP_NAME" \
        --version-string.OriginalFilename="$APP_NAME.exe" \
        --version-string.ProductName="Offline Lectures" \
        --version-string.ProductName="OLV" \
        --version-string.Copyright="2016 RogerCPA Review"

    echo ": win32 ia32 : renam dist dirs"
    chown -R $(whoami) $DIST_PATH/$PACKAGER_DIR-win32-ia32
    rm -rf $DIST_PATH/windows
    mv $DIST_PATH/$PACKAGER_DIR-win32-ia32 $DIST_PATH/windows

    echo ": win32 ia32 : dist finished"
    echo ": path: $DIST_PATH/windows"
    echo "============================="

    # for some unknown mystory of the universe, this fix's the app in some cpu's
    # yes - just moving or copying the file seems to fix it. It's related somehow
    # to the wine + rcedit (or wine + winresourcer i think) action that happens
    # somewhere in electron-packager
    mv $DIST_PATH/windows/$APP_NAME.exe $DIST_PATH/windows/START-$APP_NAME.exe
    # cp $DIST_PATH/windows/$APP_NAME.exe $DIST_PATH/windows/START-$APP_NAME.exe

    # echo ": win ia32 : prep npm modules"
    # cd app-main
    # pwd
    # npm prune --production
    # npm install --production
    # cd ..
    # pwd

    # echo ": win ia32 : electron-rebuild modules"
    # ./node_modules/.bin/electron-rebuild \
    #     --version $ELECTRON_VERSION \
    #     --node-module-version $ELECTRON_VERSION \
    #     --electron-prebuilt-dir $DIST_PATH/windows \
    #     --module-dir app-main/node_modules \
    #     --arch ia32

    # echo "patching asar with updated node_modules"
    # echo ": removing any existing asar file"
    # rm -f $DIST_PATH/windows/resources/app.asar
    # rm -f $DIST_PATH/windows/resources/default_app.asar
    # echo ': generating asar'
    # ./node_modules/.bin/asar pack app-main $DIST_PATH/windows/resources/app.asar

    # # the packer broke the .exe file, we need to fix this, and get a new one
    # # Important note: the zip we unpack comes from the electron-packer, we could download it manually
    # # like the packer, from electrons github archive
    # echo 'start patching windows exe'
    # rm -rf $TMP_PATH/win-patch
    # unzip -q -d $TMP_PATH/win-patch dist/src/electron-v$ELECTRON_VERSION-win32-ia32.zip
    # mv $DIST_PATH/windows/$APP_NAME.exe $DIST_PATH/windows/$APP_NAME.bak.exe
    # # rm -f $DIST_PATH/windows/$APP_NAME.exe
    # echo "cp -f $TMP_PATH/win-patch/electron.exe $DIST_PATH/windows/$APP_NAME.exe"
    # cp -f $TMP_PATH/win-patch/electron.exe $DIST_PATH/windows/$APP_NAME.exe
    # rm -rf $TMP_PATH/win-patch
    # echo ": done patching windows exe ($DIST_PATH/windows/$APP_NAME.exe)"

    # # update exe file if we have wine available in this *nix path
    # if [ $(which wine) ]
    # then
    #     echo ": using wine to edit exe"
    #         # --set-icon "app-main/img/icons/icon.ico" \
    #     wine ./node_modules/rcedit/bin/rcedit.exe $DIST_PATH/windows/$APP_NAME.exe \
    #         --set-version-string "CompanyName" "RogerCPA Review" \
    #         --set-version-string "FileDescription" "$APP_NAME" \
    #         --set-version-string "OriginalFilename" "$APP_NAME.exe" \
    #         --set-version-string "ProductName" "Offline Lectures"
    #     echo ": done"
    # else
    #     echo 'Skipping win32 platform because I could not find wine installed.'
    #     echo 'Try: $ brew install wine'
    #     echo 'Check: $ which wine'
    # fi

    # echo ":= prep npm modules (x64)"
    # cd app-main
    # npm prune --production
    # npm install --production
    # ../node_modules/.bin/electron-rebuild --version=$ELECTRON_VERSION \
    #     --electron-prebuilt-dir=../$ELECTRON_PREBUILT_DIR \
    #     --pre-gyp-fix \
    #     --arch=x64
    # cd ..

    # echo ":= build dist (x64)"
    # ./node_modules/.bin/electron-packager $GENERIC_FLAGS \
    #     --arch=x64 \
    #     --platform=win32 \
    #     --prune \
    #     --icon=app-main/img/icons/icon.ico \
    #     --version-string.CompanyName="Roger CPA Review" \
    #     --version-string.FileDescription="$APP_NAME" \
    #     --version-string.OriginalFilename="$APP_NAME.exe" \
    #     --version-string.ProductName="Offline Lectures" \
    #     --version-string.ProductName="OLV"

    # chown -R $(whoami) $DIST_PATH/$PACKAGER_DIR-win32-x64
    # rm -rf $DIST_PATH/windows64
    # mv $DIST_PATH/$PACKAGER_DIR-win32-x64 $DIST_PATH/windows64
fi

# disable our node, may not be required
deactivate_node
