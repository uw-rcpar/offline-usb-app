#!/bin/bash

PWD=signing/windows

SIGNING_CERT_PVX12=RogerCPAreviewSoftwareCert.p12

OUT_KEY=key.pem
OUT_PVK=authenticode.pvk
OUT_CERT=cert.pem
OUT_SPC=authenticode.spc

if [ ! -f $PWD/$OUT_PVK ]
then

    echo ''
    echo 'pass is rlouisa01'
    echo ''

    echo ''
    echo 'Extract KEY: enter password for existing p12 (pkv) file =='
    openssl pkcs12 -in $PWD/$SIGNING_CERT_PVX12 -nocerts -nodes -out $PWD/$OUT_KEY

    echo ''
    echo "Generate PVK: enter a new password for extracted $OUT_KEY file =="
    openssl rsa -in $PWD/$OUT_KEY -outform PVK -pvk-strong -out $PWD/$OUT_PVK

    echo ''
    echo 'Extract CERT: enter password for existing p12 (pkv) file =='
    openssl pkcs12 -in $PWD/$SIGNING_CERT_PVX12 -nokeys -nodes -out $PWD/$OUT_CERT

    echo ''
    echo "Generate SPC: exported cert to $OUT_SPC =="
    openssl crl2pkcs7 -nocrl -certfile $PWD/$OUT_CERT -outform DER -out $PWD/$OUT_SPC

else
    echo 'PVK / SCP files exist already.'
    echo "$PWD/$OUT_PVK"
    echo "$PWD/$OUT_SPC"
fi

echo ''
echo 'Cleanup: ensure removal of any unprotected cert/key files'
rm -f $PWD/$OUT_KEY
rm -f $PWD/$OUT_CERT
echo ''
