#!/bin/bash

PWD=signing/windows

echo 'rlouisa01' | signcode \
    -spc $PWD/authenticode.spc \
    -v $PWD/authenticode.pvk \
    -a sha1 -$ commercial \
    -n OfflineLectures \
    -i https://www.rogercpareview.com/ \
    -t http://timestamp.verisign.com/scripts/timstamp.dll \
    -tr 10 \
    dist/usb/windows/START-OfflineLectures.exe

rm -f dist/usb/windows/START-OfflineLectures.exe.bak

# certmgr
# Usage: certmgr [action] [object-type] [options] store [filename]
#    or: certmgr -list object-type [options] store
#    or: certmgr -del object-type [options] store certhash
#    or: certmgr -ssl [options] url
#    or: certmgr -put object-type [options] store certfile
#    or: certmgr -importKey [options] store pkcs12file
#
# actions
#   -add        Add a certificate, CRL or CTL to specified store
#   -del        Remove a certificate, CRL or CTL to specified store
#   -put        Copy a certificate, CRL or CTL from a store to a file
#   -list       List certificates, CRL or CTL in the specified store.
#   -ssl        Download and add certificates from an SSL session
#   -importKey  Import PKCS12 privateKey to keypair store.
# object types
#   -c      add/del/put certificates
#   -crl        add/del/put certificate revocation lists
#   -ctl        add/del/put certificate trust lists [unsupported]
# other options
#   -m      use the machine certificate store (default to user)
#   -v      verbose mode (display status for every steps)
#   -p [password]   Password used to decrypt PKCS12
#   -pem        Put certificate in Base-64 encoded format (default DER encoded)
#   -?      h[elp]  Display this help message
