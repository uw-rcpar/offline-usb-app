#!/bin/bash

# ensure we have nodeenv installed and available
USE_VIRTUAL_ENV="${USE_VIRTUAL_ENV:-true}"
DIST_PATH="${DIST_PATH:-dist/usb}"
TMP_PATH="${TMP_PATH:-dist/tmp}"
ELECTRON_PREBUILT_DIR=dist/src
PACKAGER_DIR='OfflineLectures'
APP_NAME=$PACKAGER_DIR
NODEENV_PATH="${NODEENV_PATH:-env}"
DATE=`date +%Y%m%d%H%M%S`
BUILD_VERSION="${BUILD_VERSION:-$DATE}"
APP_VERSION="${APP_VERSION:-2.0.0}"
ELECTRON_VERSION=1.4.13
DO_SIGN_MAC_BUILD="${DO_SIGN_MAC_BUILD:-yes}"
# CODE_SIGNING_IDENTITY="${CODE_SIGNING_IDENTITY:-3rd Party Mac Developer Application: Roger Philipp CPA Review (9ATMUE9N6P)}"
CODE_SIGNING_IDENTITY="${CODE_SIGNING_IDENTITY:-Developer ID Application: Roger Philipp CPA Review (9ATMUE9N6P)}"
OSX_BUNDLE_ID='com.rogercpareview.olv'

MAC_ONLY=${MAC_ONLY:-no}

echo "App Version: $APP_VERSION"
echo "Build Version: $BUILD_VERSION"
echo "Dist path: $DIST_PATH"
echo "Code Sign Mac?: $DO_SIGN_MAC_BUILD"
echo "Use Virtual Env : $USE_VIRTUAL_ENV "

# install nodeenv if not yet enabled
if [ "$USE_VIRTUAL_ENV" = true ]
then
    echo "USE Virtual Enviroment"
    if [ ! -d $NODEENV_PATH ]
    then
        nodeenv --node=5.10.1 --prebuilt $NODEENV_PATH
    fi
    # enable our nodeenv
    source $NODEENV_PATH/bin/activate
fi

npm install

# let's add our npm bin to our path
export PATH=$PATH:./node_modules/.bin

echo ': darwin x64 : code signing Mac build'
echo ""
echo "Command used (for debugging)"
echo ""
echo "export DIST_PATH=$DIST_PATH"
echo "export PACKAGER_DIR=$PACKAGER_DIR"
echo "export APP_NAME=$APP_NAME"
echo "export OSX_BUNDLE_ID=$OSX_BUNDLE_ID"
echo "export CODE_SIGNING_IDENTITY=$CODE_SIGNING_IDENTITY"
echo "export ELECTRON_VERSION=$ELECTRON_VERSION"

# APP_PATH="$DIST_PATH/$PACKAGER_DIR-darwin-x64/$APP_NAME.app"

SIGN_CMD="./node_modules/.bin/electron-osx-sign"
SIGN_CMD="$SIGN_CMD $APP_PATH"
SIGN_CMD="$SIGN_CMD --app-bundle-id=$OSX_BUNDLE_ID"
SIGN_CMD="$SIGN_CMD --identity=\"$CODE_SIGNING_IDENTITY\""
SIGN_CMD="$SIGN_CMD --platform=darwin"
SIGN_CMD="$SIGN_CMD --version=$ELECTRON_VERSION"
SIGN_CMD="$SIGN_CMD --verbose"

SIGN_CMD2="codesign --force --verify --verbose --sign"
SIGN_CMD2="$SIGN_CMD2 \"$CODE_SIGNING_IDENTITY\""
SIGN_CMD2="$SIGN_CMD2 \"$APP_PATH\""
# SIGN_CMD2="$SIGN_CMD2 --app-bundle-id=$OSX_BUNDLE_ID"
# SIGN_CMD2="$SIGN_CMD2 --platform=darwin"
# SIGN_CMD2="$SIGN_CMD2 --version=$ELECTRON_VERSION"
# SIGN_CMD2="$SIGN_CMD2 --verbose"

echo ""
echo "Signing the app with the following command"
echo ""
echo "$SIGN_CMD"
echo ""
echo "$SIGN_CMD2"
# echo $(eval $SIGN_CMD)






# electron-packager ./app-main \"Project\" --out=dist/win --platform=win32 --arch=ia32 --version=0.29.1 --icon=build/resources/icon.ico --version-string.CompanyName=\"My Company\" --version-string.ProductName=\"Project\" --version-string.FileDescription=\"Project\"

# darwin/mas target platforms only
#
# app-bundle-id      bundle identifier to use in the app plist
# app-category-type  the application category type
#                    For example, `app-category-type=public.app-category.developer-tools` will set the
#                    application category to 'Developer Tools'.
# extend-info        a plist file to append to the app plist
# extra-resource     a file to copy into the app's Contents/Resources
# helper-bundle-id   bundle identifier to use in the app helper plist
# osx-sign           (OSX host platform only) Whether to sign the OSX app packages. You can either
#                    pass --osx-sign by itself to use the default configuration, or use dot notation
#                    to configure a list of sub-properties, e.g. --osx-sign.identity="My Name"
#                    Properties supported:
#                    - identity: should contain the identity to be used when running `codesign`
#                    - entitlements: the path to entitlements used in signing
#                    - entitlements-inherit: the path to the 'child' entitlements

# disable our node, may not be required
deactivate_node

