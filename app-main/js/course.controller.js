
(function () {
    'use strict';

    myApp.controller('CourseController', function($scope, $rootScope, $stateParams, SETTINGS) {
        var courseTheme =  SETTINGS.videos[ $stateParams.courseId].theme || $stateParams.courseId;
        // if the name contains '-CRAM' can safely strip that off (in case the theme property is not in the JSON file)
        courseTheme = courseTheme.replace("-CRAM", "");
        $rootScope.pageClass = "page-course page-course-"+courseTheme
        $rootScope.course = $stateParams.courseId
        $rootScope.courseClass = courseTheme;
        console.debug("CourseController courseClass", $rootScope.courseClass )
        $scope.title = $stateParams.courseId
        $scope.listChapters = function(course) {
            return SETTINGS.videos[course]['chapters']
        }

    })
})();

