(function () {
    'use strict';
    myApp.controller('LoginController', function($scope, $location , $state, $rootScope, AuthenticationService) {
        $scope.dataLoading = false ;
        $scope.username = '';
        $scope.password = '';
        $scope.global_error  = false ;
        $scope.username_error = false ;
        $scope.password_error = false ;

        $scope.login = function () {
            var self = this
            self.dataLoading = true;

            // dataLoading = false;
            AuthenticationService.Login(self.username, self.password, $rootScope.course, function (response) {
                
                //$scope.error = true ;
                self.dataLoading = false
                if (response.success) {
                    AuthenticationService.SetCredentials(self.username, self.password, $rootScope.course, response.entitlement).then(function(){
                        $scope.$close();
                    });
                } else {
                    $scope.global_error = true ;
                    $scope.username_error = true ;
                    $scope.password_error = true ;
                    $scope.global_error_message = response.message ;
                    self.dataLoading = false;
                }
            });
        };


        $scope.logout = function () {
            AuthenticationService.Logout(function(){
                $state.go('login');
                
            })
        };

    });

})();

