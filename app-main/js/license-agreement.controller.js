
(function () {
    'use strict';

    myApp.controller('LicenseAgreementController', function($scope, $rootScope, $stateParams, $uibModalInstance, $uibModalStack, AuthenticationService) {
        $scope.accept = function() {
            var course = $rootScope.course
            $rootScope.currentUser.acceptedLicenses[course] = true ;
            // TODO save into database
            $scope.$close();
            $uibModalInstance.close({});
            $uibModalStack.dismissAll();
            AuthenticationService.saveUserData().then(function(){
                $uibModalInstance.close();
            })
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }

        $scope.openLegal =  function() {
            var url = 'http://www.rogercpareview.com/legal/usb';
            console.log("Opening external url", url);
            require('shell').openExternal(url);

        }
    })
})();
