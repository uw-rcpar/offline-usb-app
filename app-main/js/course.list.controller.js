(function () {
    'use strict';

    myApp.controller('CourseListController', function($scope, $rootScope, SETTINGS) {
        $rootScope.pageClass = "page-entrance" ;

        // Avoid infidg by setting the array here
        //https://docs.angularjs.org/error/$rootScope/infdig
        $scope.courses = []

        $scope.defaultChapter = function (courseId) {
            console.debug('get default chapter: ' + courseId + ' = ' + Object.keys(SETTINGS.videos[courseId].chapters)[0])
            console.debug(SETTINGS.videos[courseId].chapters)
            // return the first element of the array (object)
            return Object.keys(SETTINGS.videos[courseId].chapters)[0];
        }

        angular.forEach(SETTINGS.videos, function(value,key){
            console.log(key)
            $scope.courses.push({
                id: key,
                theme: value.theme || key.replace('-CRAM', ''),
                text: key
            })
        })
    })
})();
