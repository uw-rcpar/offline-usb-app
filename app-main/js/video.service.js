(function () {
    'use strict';

    angular
        .module('myApp')
        .factory('VideoService', VideoService);

    //VideoService.$inject = [];
    function VideoService(SETTINGS) {
        var service = {};
        service.listVideos = listVideos;
        service.getVideoById = getVideoById;
        service.getEntitlement = getEntitlement;
        return service;

        /**
         * List videos
         */
        function listVideos(course, chapter) {
            if ( course && chapter ) {
                return SETTINGS.videos[course]['chapters'][chapter]['topics'] ;
            }
        }

        function getVideoById(id) {
            var video = null ;
            angular.forEach(SETTINGS.videos, function(chapters) {
                angular.forEach(chapters['chapters'], function(topics) {
                    if ( typeof topics['topics'][id] != 'undefined' ){
                        video =  topics['topics'][id] ;
                    }
                });
            });
            console.debug(video)
            return video ;
        }

        function getEntitlement(course) {
            if (typeof course != 'undefined') {
                var promise = new Promise(function (resolve, reject) {

                    var transaction = db.transaction(["entitlements"], "readwrite");
                    var store = transaction.objectStore("entitlements");
                    // Store username on global scope
                    // TODO SAve on db

                    store.get(course).onsuccess = function (event) {
                        resolve(this.result)
                    }


                    store.get(course).onerror = function (event) {
                        reject()
                    }

                })
                return promise
            }
        }


    }
})();


