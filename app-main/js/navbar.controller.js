(function () {
    'use strict';
    myApp.controller('NavBarController', function($rootScope, $state, $scope, $uibModalStack) {
        $scope.isCollapsed = true;

        $scope.home = function () {
            console.debug($rootScope);
            $rootScope.course = null ;
            $rootScope.courseClass = '' ;
            $state.go('courses',{},{reload : true});
            $uibModalStack.dismissAll();
        }

        $scope.closeModals = function(){
            $uibModalStack.dismissAll();
        }

        $scope.switchCourse = function (course ) {
            $uibModalStack.dismissAll();
            $state.go('course',{courseId: course });
        }

    })
})();