(function () {
    'use strict';
    var remote = require('electron').remote;
    var app = require('electron').remote.app;
    var path = require('path')


    myApp.controller('VideoListController', function($scope, $rootScope, $stateParams, $uibModal, VideoService) {
        var course, chapter ;
        var course = $rootScope.course ;
        chapter = $stateParams.chapterId ;
        $rootScope.chapter = chapter ;
        $scope.videos = VideoService.listVideos(course, chapter)

        var modalInstance ;
        $scope.openModal = function(videoId) {
            var video_suffix = ".crypt.webm" ; // If the json says: 'AUD-0.01.mp4' the filesystem name is 'AUD-0.01.mp4.crypt.webm'
            var src, folder, filename, filename;
            var binPath = app.getPath('exe')
            var filename = path.basename(binPath)
            var video = VideoService.getVideoById(videoId)
            if ( video ) {
                if (filename == 'electron' || filename == 'electron.exe'){// IF Node dev mode just relative to the index.html
                    var src = "../../videos/"+video.video_file+video_suffix
                }else{

                    console.debug('getPath(exe)', app.getPath('exe'));
                    console.debug('platform', process.platform);
                    // folder = path.join ( path.dirname(binPath) , '..' , 'videos')
                    if (process.platform == 'darwin') {
                        // mac has a deeper bin dir, inside the .app dir
                        var src = "../../../../../videos/"+video.video_file+video_suffix
                        folder = path.join ( path.dirname(binPath), '..', '..', '..', '..', '.videos')
                    } else {
                        // relative to the .exe file
                        folder = path.join ( path.dirname(binPath) , '..' , '.videos')
                    }

                    src = fileUrl(path.join( folder ,  video.video_file+video_suffix))
                }
                $scope.video_src = src
                $scope.video = video ;//passed current scope to the modal

                VideoService.getEntitlement(course).then(function (entitlement) {
                    if (entitlement) {
                        initVideo(entitlement.license_key)
                    }else{
                        console.error('Error Loading license for ' + course );
                    }
                })
            }else{
            }

            modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'video.html',
                //controller: 'VideoViewController',
                windowClass: 'video-play-modal nav-visible',

                scope: $scope,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                },
            });

            modalInstance.result.then(function (selectedItem) {
            }, function () {
                $scope.video_src = '' ;
                $scope.video_id = '' ;//passed current scope to the modal
            });

        };

        $scope.closeModal = function() {
            modalInstance.dismiss('cancel');
        };
    })



    //var fileUrl = require('file-url'); not sure why windows version do not find this lib
    // So copied and pasted the code snipped here

    function fileUrl(str) {
        if (typeof str !== 'string') {
            throw new Error('Expected a string');
        }

        var pathName = path.resolve(str).replace(/\\/g, '/');

        // Windows drive letter must be prefixed with a slash
        if (pathName[0] !== '/') {
            pathName = '/' + pathName;
        }

        return encodeURI('file://' + pathName);
    };




    function _removeLastDirectoryPartOf(the_url)
    {
        var the_arr = the_url.split('/');
        the_arr.pop();
        return( the_arr.join('/') );
    }

    function initVideo(license_key) {
        //license_key = "Pk5Rh1ZkXkERKoBoMwp+Sg" ; //REG
        //license_key = "qUhM17/COnWS43I/4qMd3A" ; // AUD

        console.debug('license key: '+ license_key)
        var B64_KEY = license_key  ;
        var config = [{
            initDataTypes: ['webm'],
            videoCapabilities: [{
                contentType: 'video/webm; codecs="vp8"'
            }]
        }];

        var video = document.querySelector('video');
        video.addEventListener('encrypted', handleEncrypted, false);

        navigator.requestMediaKeySystemAccess('org.w3.clearkey', config).then(
            function (keySystemAccess) {
                return keySystemAccess.createMediaKeys();
            }
        ).then(
            function (createdMediaKeys) {
                return video.setMediaKeys(createdMediaKeys);
            }
        ).catch(
            function (error) {
                console.error('Failed to set up MediaKeys', error);
            }
        );

        function handleEncrypted(event) {

            var session = video.mediaKeys.createSession();
            session.addEventListener('message', handleMessage, false);
            session.generateRequest(event.initDataType, event.initData).catch(
                function (error) {
                    console.error('Failed to generate a license request', error);
                }
            );
        }

        function handleMessage(event) {
            // If you had a license server, you would make an asynchronous XMLHttpRequest
            // with event.message as the body.  The response from the server, as a
            // Uint8Array, would then be passed to session.update().
            // Instead, we will generate the license synchronously on the client, using
            // the hard-coded KEY at the top.
            var license = generateLicense(event.message);

            var session = event.target;
            session.update(license).catch(
                function (error) {
                    console.error('Failed to update the session', error);
                }
            );
        }

        // Convert Uint8Array into base64 using base64url alphabet, without padding.
        function toBase64(u8arr) {
            return btoa(String.fromCharCode.apply(null, u8arr)).replace(/\+/g, '-').replace(/\//g, '_').replace(/=*$/, '');
        }

        // This takes the place of a license server.
        // kids is an array of base64-encoded key IDs
        // keys is an array of base64-encoded keys
        function generateLicense(message) {
            // Parse the clearkey license request.
            var request = JSON.parse(new TextDecoder().decode(message));
            // We only know one key, so there should only be one key ID.
            // A real license server could easily serve multiple keys.
            //var keyb64 = toBase64(KEY)

            var keyObj = {
                kty: 'oct',
                alg: 'A128KW',
                kid: request.kids[0],
                k: B64_KEY
            };
            return new TextEncoder().encode(JSON.stringify({
                keys: [keyObj]
            }));
        }
    }






})();
