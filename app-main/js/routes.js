// Routing
myApp.config(function($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/courses");


    $stateProvider.state('courses', {
        url: "/courses",
        templateUrl: "course.list.html",
        controller: 'CourseListController',
        data: {
            requireLogin: false,
            requireLicenseAgreement: false,
        }
    })

    $stateProvider.state('course', {
        url: "/course/:courseId",
        templateUrl: "course.html",
        controller: 'CourseController',
        data: {
            requireLogin: true,
            requireLicenseAgreement: true,
        }
    })
    .state('course.chapter', {
        url: "/:chapterId",
        templateUrl: "video.list.html",
        controller: 'VideoListController'
    })
    .state('course.chapter.video', {
        url: "/:videoId",
        templateUrl: "video.html",
        controller: 'VideoViewController'
    });




});