// LoginModalCtrl.js
myApp.service('loginModal', function ($uibModal, $rootScope) {

    return function() {
        var instance = $uibModal.open({
            templateUrl: 'login.html',
            controller:   'LoginController',
            controllerAs: 'LoginController',
            windowClass: 'login-modal nav-visible',
            backdrop  : 'static',
            keyboard  : false
        })
        return instance.result;
    };

});



// LoginModalCtrl.js
myApp.service('licenseAgreementModal', function ($uibModal, $rootScope) {
    return function() {
        var instance =   $uibModal.open({
            templateUrl: 'license-agreement.html',
            controller:   'LicenseAgreementController',
            controllerAs: 'LicenseAgreementController',
            windowClass: 'license-agreement-modal nav-visible',
        })
        return instance.result;
    };

});

