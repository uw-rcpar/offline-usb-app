(function () {
    'use strict';

    angular
        .module('myApp')
        .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$q', '$http' , '$rootScope', 'SETTINGS'];
    function AuthenticationService($q, $http,  $rootScope, SETTINGS) {
        var service = {};
        service.Login = Login;
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;
        service.Logout = Logout;
        service.isAuthenticated = isAuthenticated ;
        service.loadSavedUserData = loadSavedUserData ;
        service.initUser = initUser ;
        service.licenseAccepted = licenseAccepted ;
        service.saveUserData = saveUserData ;
        return service;

        function Login(username, password, course, callback) {
            var base_args = {username: username, password: password, course: course} ;
            if (SETTINGS.auth_extra_args){
                console.debug(SETTINGS.auth_extra_args);
                for (var k in SETTINGS.auth_extra_args){
                    base_args[k] = base_args[k] || SETTINGS.auth_extra_args[k];
                }
            }
            $http.post(SETTINGS.service_auth_url, base_args)
                .success(
                    function (response) {
                        callback(response);
                    })
        }

        function Logout (callback) {
            ClearCredentials()
            callback(callback);

        }


        function SetGlobalUserData(username, password, course, entitlement) {
            var promise = new Promise(function(resolve, reject) {

                // SEt global variable
                if (typeof $rootScope.currentUser == 'undefined') {
                    $rootScope.currentUser = {
                        username: null,
                        allowedCourses: {},
                        acceptedLicences: {}
                    }
                }
                $rootScope.currentUser.username = username;
                $rootScope.currentUser.allowedCourses[course] = true

                // Write user to db and resolve promise
                saveUserData().then(function(){
                    // Save entitlement:
                    var store = db.transaction(["entitlements"], "readwrite").objectStore("entitlements");
                    store.put(entitlement, course).onsuccess = function () {
                        resolve();
                    }
                })
            });
            return promise ;
        }

        function SetCredentials(username, password,course, entitlement) {
            var promise = new Promise(function(resolve, reject) {
                // Store username on global scope
                SetGlobalUserData(username, password, course, entitlement).then(function () {
                    resolve()
                });
            });
            return promise
        }

        function ClearCredentials() {
            $rootScope.currentUser = {};
            $this.user = null  ;
        }

        function isAuthenticated(){
            return $rootScope.currentUser != null ;
        }

        function loadSavedUserData() {
            var deferred = $q.defer();

            deferred.resolve();

            var transaction = db.transaction(["globals"], "readwrite");
            var store = transaction.objectStore("globals");

            var request = store.get('currentUser')
            request.onsuccess =     function (event) {
                // Load current user
                var res = event.target.result
                if ( res) {
                    $rootScope.currentUser = res ;
                }

                // Load entitlements and validate license expiration
                var request2 = db.transaction(["entitlements"], "readwrite").objectStore("entitlements").getAll()
                request2.onsuccess =     function (event) {
                    // Load current user
                    var res2 = event.target.result
                    // validate or invalidate entitlements based on expiration date
                    angular.forEach(res2, function (e, i) {
                        if (e.expires_on > Math.floor(Date.now() / 1000)) {
                            $rootScope.currentUser.allowedCourses[e.course] = true
                        } else {
                            $rootScope.currentUser.allowedCourses[e.course] = false
                            alert(e.course + ' licence expired.');
                        }
                    })
                }
            }


            return deferred.promise

        }

        function initUser(){
            $rootScope.currentUser = {
                acceptedLicenses: {},
                allowedCourses: {},
                username: null
            }
        }

        function licenseAccepted(course) {
            console.debug('licAccepted ? ' + course ) ;
            if ( course ) {
                return typeof $rootScope.currentUser.acceptedLicenses[course] != "undefined" && $rootScope.currentUser.acceptedLicenses[course]
            }else{
                console.debug('course is not valid arg');
                return false ;
            }
        }

        function saveUserData(){
            var promise = new Promise(function(resolve, reject) {
                // Store username on global scope
                db.transaction(["globals"], "readwrite").objectStore("globals").put($rootScope.currentUser, 'currentUser').onsuccess = function () {
                    resolve();
                }
            })
            return promise
        }


    };

})();
