var idbSupported = false;
var db;
var openRequest;
var remote = require('electron').remote;
var myApp = angular.module('myApp', ['ui.router', 'ui.bootstrap']);
var settings =  remote.getGlobal('settings');

//myApp.constant("SERVICE_AUTH_URL", "https://www.rogercpareview.com/user/ssologin/offline-lec");
myApp.constant("SETTINGS", settings);


myApp.run(function ($rootScope, $state, loginModal, licenseAgreementModal, AuthenticationService, SETTINGS) {
    // Init database
    var app = require('electron').remote.app,
        fsExtra = require('fs-extra'),
        path = require('path');
    console.debug('getPath(exe)', app.getPath('exe'));
    console.debug('platform', process.platform);
    console.debug(window.location.href)
    AuthenticationService.initUser()

    if("indexedDB" in window) {
        idbSupported = true;
    }

    if(idbSupported) {
        openRequest = indexedDB.open("lectures",4);

        openRequest.onupgradeneeded = function(e) {
            var thisDB = e.target.result;
            //
            if(!thisDB.objectStoreNames.contains("globals")) {
                thisDB.createObjectStore("globals");
            }

            //
            if(!thisDB.objectStoreNames.contains("entitlements")) {
                thisDB.createObjectStore("entitlements");
            }


        }

        openRequest.onsuccess = function(e) {
            db = e.target.result;
            AuthenticationService.loadSavedUserData()
        }

        openRequest.onerror = function(e) {
            console.debug(e)
            alert("Database error: " + e.target.errorCode);

            console.log("Error");
            console.dir(e);
        }
    }

    // Of display_chapters is not defined set to true by default
    $rootScope.displayChapters =  typeof SETTINGS.display_chapters != 'undefined' ?  SETTINGS.display_chapters : true
    $rootScope.$on('$stateChangeSuccess',function (event, toState, toParams, fromState, fromParams){
        console.debug("LOCATION: " + window.location.href )
    })


    // Listen route changes login acces request etc
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        //alert('chage start')
        var requireLogin = toState.data.requireLogin;
        var requireLicenceAgreement = toState.data.requireLicenseAgreement;
        var licAccepted = false ;

        if(toParams.courseId) {
            // TODO: Wrap rootScope course handiling in a service
            var courseTheme = SETTINGS.videos[ toParams.courseId].theme || toParams.courseId ;
            courseTheme = courseTheme.replace("-CRAM", "");
            // $rootScope.pageClass = "page-course page-course-"+courseTheme
            $rootScope.course = toParams.courseId
            $rootScope.courseClass = courseTheme
            licAccepted = AuthenticationService.licenseAccepted($rootScope.course)

/*            if(toParams.courseId && !toParams.chapterId) {
                // var firstChapter = "AUD-CRAM-0"
                return $state.go('course.chapter', {chapterId:"AUD-CRAM-0", courseId:  toParams.courseId}, {reload: true})
            }*/
        }

        if (requireLicenceAgreement && !licAccepted  ) {
            event.preventDefault();
            licenseAgreementModal()
                .then(function () {
                    return $state.go(toState.name, toParams);
                })
                .catch(function () {
                    return $state.go('courses');
                });
        }else {
            // License accepted or not required
            if (requireLogin && ( typeof $rootScope.currentUser === 'undefined' || !$rootScope.currentUser.allowedCourses[$rootScope.course] )) {
                event.preventDefault();

                loginModal()
                    .then(function () {
                        return $state.go(toState.name, toParams);
                    })
                    .catch(function () {
                        return $state.go('courses');
                    });
            }
        }

    });

});
