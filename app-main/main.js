// Read app version from APP_VERSION env variable
// Allowed values: cram: flash cram videos or empty for default (offlines lectures)
// This will load the corresponding settings file
// The naming conversion for the settings file is: "settings-[APP_VERSION].json"

const {app, BrowserWindow} = require('electron')
const electron = require('electron')
const fs = require('fs')
var path = require('path')

// Module to control application life.
// const app = electron.app
// // Module to create native browser window.
// const BrowserWindow = electron.BrowserWindow

// Keep a global reference of the window object, if you don't, mthe window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow




var settingsFile =__dirname + '/settings.json'
fs.openSync(settingsFile, 'r+'); //throws error if file doesn't exist
var data=fs.readFileSync(settingsFile); //file exists, get the contents
global.settings = JSON.parse(data);


// Setup app data path reading from settings json
// C:\Users\<yourUser>\AppData\Roaming\OfflineLectures FULL COURSES
// C:\Users\<yourUser>\AppData\Roaming\OfflineLectures-CRAM CRAM COURSES
var defaultUserDataPath = app.getPath('userData');
var userDataPathParts = path.parse(defaultUserDataPath);
var newUserDataPath = path.join(userDataPathParts.dir, global.settings.user_data_dir);
app.setPath('userData', newUserDataPath) ;



function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 1024, height: 768})

  // and load the index.html of the app.
  mainWindow.loadURL('file://' + __dirname + '/views/index.html#/courses')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}


// This method makes your application a Single Instance Application -
// instead of allowing multiple instances of your app to run,
// this will ensure that only a single instance of your app is running,
// and other instances signal this instance and exit.
var shouldQuit = app.makeSingleInstance(function(commandLine, workingDirectory) {
  // Someone tried to run a second instance, we should focus our window.
  if (mainWindow) {
    if (mainWindow.isMinimized()) mainWindow.restore();
    mainWindow.focus();
  }
});
if (shouldQuit) {
  app.quit();
  return;
}





// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all window
// s are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
